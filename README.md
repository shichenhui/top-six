<!--
 * @Author: your name
 * @Date: 2020-11-09 18:40:03
 * @LastEditTime: 2020-11-24 07:32:48
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \top-six\README.md
-->
# TopSix
## 第六组项目进度
### 倪建军
#### 2020.11.24
- 文章阅读
   - [Umi hooks =>aHooks 实用代码片段整理](https://juejin.cn/post/6844904187206041608)
- 源码阅读
   - [发布 umi 2.0，可插拔的企业级 react 应用框架](https://juejin.cn/post/6844903668634877966)
- 力扣刷题
   -[49. 字母异位词分组](https://leetcode-cn.com/problems/group-anagrams/)
- 项目进度
#### 2020.11.19
- 文章阅读
   - [umi-request](https://juejin.cn/post/6844903982867939342)
- 源码阅读
   - [与 fetch、axios 的异同？](https://juejin.cn/post/6844903982867939342)
- 力扣刷题
   - [175. 组合两个表](https://leetcode-cn.com/problems/combine-two-tables/)
- 项目进度
   - 使用umi框架搭建项目
   - 实现页面轮播效果
   - 实现路由
   - 实现登陆页面
   - 使用less样式排版
   - 请求数据
#### 2020.11.18
- 文章阅读
   - [umi-request 网络请求之路](https://juejin.im/post/6844903982867939342)
- 源码阅读
  - [搭建 umi + qiankun + antd 的微前端平台](https://juejin.im/post/6869220236886245383)
- 力扣刷题
  - [48. 旋转图像](https://leetcode-cn.com/problems/rotate-image/)
#### 2020.11.17
- 文章阅读
   - [基于Umi的开发方案](https://juejin.im/post/6844903886029848589)
- 源码阅读
   - [Umi开发Chrome扩展记录](https://juejin.im/post/6882623435396694023)
- 力扣刷题
   - [39. 组合总和](https://leetcode-cn.com/problems/combination-sum/)
#### 2020.11.16
- 源码阅读
   - [React List - React Router](https://juejin.im/post/6844903833043206158)
- 文章阅读
   - [react-router 源码浅析](https://juejin.im/post/6844903618580217864)
- 力扣刷题
   - [字符串转换整数 (atoi)|8题](https://leetcode-cn.com/problems/string-to-integer-atoi/)
   - [字符串转换整数 (atoi)|7题](https://leetcode-cn.com/problems/reverse-integer/)
#### 2020.11.12
- 源码阅读
   - [React、Redux、React-Redux](https://juejin.im/post/6891185214911938573)
- 文章阅读
   - [用原生实现Vue3](https://juejin.im/post/6893880467305529352) 
- Leecode刷题
   - [电话号码的字母组合](https://leetcode-cn.com/problems/letter-combinations-of-a-phone-number/)
   - [四数之和](https://leetcode-cn.com/problems/4sum/)
- 项目进度
  - 排版完成
  - vue3.0数据渲染
  - 请求数据完成
  - 封装axios拦截器
  -  tab切换
  - 详情页面
#### 2020.11.11
- 项目进度
  - 排版完成
  - vue3.0数据渲染
  - 请求数据完成
  - 封装axios拦截器
  -  tab切换
  - 详情页面
#### 2020.11.16
- 源码阅读
   - [React List - React Router](https://juejin.im/post/6844903833043206158)
- 文章阅读
   - [react-router 源码浅析](https://juejin.im/post/6844903618580217864)
- 力扣刷题
   - [字符串转换整数 (atoi)|8题](https://leetcode-cn.com/problems/string-to-integer-atoi/)
   - [字符串转换整数 (atoi)|7题](https://leetcode-cn.com/problems/reverse-integer/)
#### 2020.11.12
- 源码阅读
   - [React、Redux、React-Redux](https://juejin.im/post/6891185214911938573)
- 文章阅读
   - [用原生实现Vue3](https://juejin.im/post/6893880467305529352) 
- Leecode刷题
   - [电话号码的字母组合](https://leetcode-cn.com/problems/letter-combinations-of-a-phone-number/)
   - [四数之和](https://leetcode-cn.com/problems/4sum/)
- 项目进度
  - 排版完成
  - vue3.0数据渲染
  - 请求数据完成
  - 封装axios拦截器
  -  tab切换
  - 详情页面
#### 2020.11.11
- 项目进度
  - 排版完成
  - vue3.0数据渲染
  - 请求数据完成
  - 封装axios拦截器
  -  tab切换
  - 详情页面
#### 2020.11.11
- 项目进度
  - 排版完成
  - vue3.0数据渲染
  - 请求数据完成
  - 封装axios拦截器
  -  tab切换
  - 详情页面
#### 2020.11.10
- 项目进度
  - 排版完成
  - vue3.0数据渲染
  - 请求数据完成
  - 封装axios拦截器
#### 2020.11.9
- 项目进度
   - vue3.0下载脚手架
  - 请求数据
  今天遇到的问题：
  vue3.0不太会
- 文章阅读
   - [vue2.0构建的一个文章阅读应用](https://blog.csdn.net/weixin_34218890/article/details/89140903)
   - [vue实现文章内容过长点击阅读全文功能的实例](https://www.kunju   ke.com/jiaocheng/38546/)
- leetcode刷题
   - 第50题
   - 第51题
- 源码阅读
   - [微信小程序之添加文章功能](https://blog.csdn.net/weixin_44387746/article/details/107085871)
   - [node](https://juejin.im/post/6844903778194292744) 

### 师晨珲
#### 2020.11.23
  - 文章阅读
    + [Vue3 demo初体验笔记 -Vue3跟Vue区别](https://juejin.cn/post/6896636024186011655)
    + [vue3 学习 之 vue3使用](https://juejin.cn/post/6896438269291347976)
  - leeCode刷题
    + 第46题[全排列](https://leetcode-cn.com/problems/permutations/)
    + 第47题[全排列 II](https://leetcode-cn.com/problems/permutations-ii/)
  - 项目进度
    + 
  - 遇到的问题
    + 
  - 源码阅读
    + [Web 架构师如何做性能优化？](https://juejin.cn/post/6898235695245197325)
    + [这些开源项目，让你轻松应对十大工作场景](https://juejin.cn/post/6898098763772985352)
#### 2020.11.20
  - 文章阅读
    + [快速使用Vue3最新的15个常用API](https://juejin.cn/post/6897030228867022856)
    + [6个规则去净化你的代码](https://juejin.cn/post/6897100630494543886)
  - leeCode刷题
    + 第44题[通配符匹配](https://leetcode-cn.com/problems/wildcard-matching/)
    + 第45题[跳跃游戏 II](https://leetcode-cn.com/problems/jump-game-ii/)
  - 项目进度
    + 使用umi框架
    + 获取classify页面数据，使用接口
    + 完成classify页面排版
    + 点击每一项跳转页面
    + 获取tab数据，完成tab切换，跳转tab页面
    + 数据渲染
    + 跳转详情页，渲染页面
  - 遇到的问题
    + 
  - 源码阅读
    + [自己整理的一些面试题](https://juejin.cn/post/6897093593929154567)
    + [我们把公司前端架构了！](https://juejin.cn/post/6897103348227391496)
#### 2020.11.19
  - 文章阅读
    + [umi-request 网络请求之路](https://juejin.cn/post/6844903982867939342)
    + [Umi hooks =>aHooks 实用代码片段整理](https://juejin.cn/post/6844904187206041608)
  - leeCode刷题
    + 第42题[接雨水](https://leetcode-cn.com/problems/trapping-rain-water/)
    + 第43题[字符串相乘](https://leetcode-cn.com/problems/multiply-strings/)
  - 项目进度
    + 使用umi框架
    + 获取classify页面数据，使用接口
    + 完成classify页面排版
    + 点击每一项跳转页面
    + 获取tab数据
  - 遇到的问题
    + 
  - 源码阅读
    + [浅谈umi3的微内核体系](https://juejin.cn/post/6844904089231458317)
    + [从零开始创建一个React(Umi+AntD)项目](https://juejin.cn/post/6844904184597184519)
#### 2020.11.18
  - 文章阅读
    + [Hello！umi](https://juejin.im/post/6844903557783633927)
    + [基于Umi的开发方案](https://juejin.im/post/6844903886029848589)
  - leeCode刷题
    + 第40题[ 组合总和 II](https://leetcode-cn.com/problems/combination-sum-ii/)
    + 第41题[缺失的第一个正数](https://leetcode-cn.com/problems/first-missing-positive/)
  - 项目进度
    + 使用umijs开发
    + 完成TodoList开发
    + 获取首页数据
    + 渲染数据
  - 遇到的问题
    + 获取数据
    + 排版
  - 源码阅读
    + [umi](https://github.com/umijs/umi/tree/master/packages/umi#readme)
#### 2020.11.17
  - 文章阅读
    + [编写高质量可维护的代码——异步优化](https://juejin.im/post/6896028236732776461)
    + [我用 10 张脑图，征服了一系列大厂面试官。](https://juejin.im/post/6895889342204493831)
  - leeCode刷题
    + 第38题[外观数列](https://leetcode-cn.com/problems/count-and-say/)
    + 第39题[组合总和](https://leetcode-cn.com/problems/combination-sum/)
  - 项目进度
    + 使用umijs开发
    + 完成TodoList开发
  - 遇到的问题
    + 
  - 源码阅读
    + [[React Hooks 翻译] 5-8 Hook规则](https://juejin.im/post/6844903853947617288)
    + [React Hooks](https://juejin.im/post/6844903999993446413)
#### 2020.11.16
  - 文章阅读
    + [【优化】记一次通过工具减少 Git 冲突](https://juejin.im/post/6895534290411454477)
    + [Vue 全家桶实现网易云音乐 WebApp](https://juejin.im/post/6844903606332882957)
  - leeCode刷题
    + 第36题[有效的数独](https://leetcode-cn.com/problems/valid-sudoku/)
    + 第37题[解数独](https://leetcode-cn.com/problems/sudoku-solver/)
  - 项目进度
  - 遇到的问题
  - 源码阅读
    + [[React Hooks 翻译] 3-8 State Hook](https://juejin.im/post/6844903853943422984)
    + [[React Hooks 翻译] 4-8 Effect Hook](https://juejin.im/post/6844903853943422989)
#### 2020.11.15
  - 文章阅读
    + [🔥基于React全家桶开发「网易云音乐PC」项目实战(一)](https://juejin.im/post/6893817287917338632)
    + [基于React全家桶开发「网易云音乐PC」项目实战(二)](https://juejin.im/post/6894914653479960583)
  - leeCode刷题
    + 第34题[在排序数组中查找元素的第一个和最后一个位置](https://leetcode-cn.com/problems/find-first-and-last-position-of-element-in-sorted-array/)
    + 第35题[搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)
  - 项目进度
  - 遇到的问题
  - 源码阅读
    + [[React Hooks 翻译] 1-8 介绍Hooks](https://juejin.im/post/6844903850659282957)
    + [[React Hooks 翻译] 2-8 初探Hooks](https://juejin.im/post/6844903850676060168) 
#### 2020.11.13
  - 文章阅读
    + [Vue 3.0 Ref-sugar 提案真的是自寻死路吗？](https://juejin.im/post/6894175515515551752)
    + [试试前端自动化测试！（React 实战）](https://juejin.im/post/6894234532224958478)
  - leeCode刷题
    + 第32题  [最长有效括号](https://leetcode-cn.com/problems/longest-valid-parentheses/)
    + 第33题  [ 搜索旋转排序数组](https://leetcode-cn.com/problems/search-in-rotated-sorted-array/)
  - 项目进度
    + 搭建vue3.0项目
    + 使用vant 库
    + 前后端交互，获取数据
    + 封装axios
    + 分类列表，tab切换
    + 跳详情，渲染数据
    + 个人页面
  - 遇到的问题
  - 源码阅读
    + [diff算法](https://www.cnblogs.com/wind-lanyan/p/9061684.html)
    + [用原生实现Vue3](https://juejin.im/post/6893880467305529352?utm_source=gold_browser_extension)
#### 2020.11.12
  - 文章阅读
    + [vue2.0构建的一个文章阅读应用](https://blog.csdn.net/weixin_34218890/article/details/89140903)
    + [vue实现文章内容过长点击阅读全文功能的实例](https://www.kunjuke.com/jiaocheng/38546/)
  - leeCode刷题
    + 第30题  [两数相除](https://leetcode-cn.com/problems/divide-two-integers/)
    + 第31题  [ 串联所有单词的子串](https://leetcode-cn.com/problems/substring-with-concatenation-of-all-words/)
  - 项目进度
    + 搭建vue3.0项目
    + 使用vant 库
    + 前后端交互，获取数据
    + 封装axios
    + 分类列表，tab切换
    + 跳详情，渲染数据
  - 遇到的问题
  - 源码阅读
    + [vue3的变化](https://blog.csdn.net/qq_41107410/article/details/106132744)
    + [Vue3+TS体验并开发+Vite浅析](https://juejin.im/post/6887478361623527431?utm_source=gold_browser_extension)
#### 2020.11.11
#### 2020.11.09
  - 文章阅读
    + [这些高阶的函数技术，你掌握了么](https://juejin.im/post/6892886272377880583)
    + [🎉🎉一个基于 Vue 3 + Vant 3 的开源商城项目🎉🎉](https://juejin.im/post/6892783570016796679)
  - leeCode刷题
    + 第25题  [K 个一组翻转链表](https://leetcode-cn.com/problems/reverse-nodes-in-k-group/)
    + 第26题  [删除排序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
  - 项目进度
    + 搭建vue3.0项目
    + 使用vant 库
    + 前后端交互，获取数据
    + 封装axios
  - 遇到的问题
  - 源码阅读
    + [一个实用的Chrome小工具：xTrace](https://juejin.im/post/6893004236594872334)
#### 2020.11.10
  - 文章阅读
    + [小蝌蚪传记：前端菜鸟让接口提速60%的优化技巧](https://juejin.im/post/6893286451711049742)
    + [前端搞算法再也不难，如何套路解题：滑动窗口类](https://juejin.im/post/6893435960705417224)
  - leeCode刷题
    + 第27题  [移除元素](https://leetcode-cn.com/problems/remove-element/)
    + 第28题  [实现 strStr()](https://leetcode-cn.com/problems/implement-strstr/)
  - 项目进度
    + 搭建vue3.0项目
    + 使用vant 库
    + 前后端交互，获取数据
    + 封装axios
    + 分类列表，tab切换
  - 遇到的问题
  - 源码阅读
    + [「JS」浅析 JS 原型链和继承](https://juejin.im/post/6893440135841775623)
### 樊鹏飞
#### 11-18
   - 文章阅读
      + [SPA 路由三部曲之核心原理](https://juejin.im/post/6895882310458343431)
      + [React Hook丨用好这9个钩子，所向披靡](https://juejin.im/post/6895966927500345351)
   - 力扣刷题
      + 289. 生命游戏
      + 295. 数据流的中位数
   - 项目进度
      + 路由配置
      + 数据请求
      + 轮播图
#### 11-17
   - 文章阅读
      + [javascript的基础清单](https://juejin.im/post/6895930107139981325)
      + [Vue 项目一些常见问题的解决方案](https://juejin.im/post/6895497352120008717)
   - 力扣刷题
      + [257. 二叉树的所有路径]   
      + [287. 寻找重复数]
   - 源码阅读
      + [基于Umi的开发方案](https://juejin.im/post/6844903886029848589)   
   - 项目进度
      + Umi框架
      + 完成Todolist   
#### 11-12
   - 文章阅读
      + [用原生实现Vue3，真香~](https://juejin.im/post/6893880467305529352)
      + [VS Code 折腾记 - (19) 一些相对实用的编码体验插件(偏前端)](https://juejin.im/post/6894117368121786382)
   - 力扣刷题
      + [相同的树](https://leetcode-cn.com/problemset/all/) 
      + [对称二叉树](https://leetcode-cn.com/problemset/all/)
   - 源码阅读
      + [React源码分析](https://juejin.im/post/6844903586527182861)
   - 项目进度(神树排行)  
      + 页面渲染
      + 跳转详情
      + 视频播放
      + 分类页面tab切换,切换对应数据
#### 11-11
   - 文章阅读
      + [Vue3 模板编译优化](https://juejin.im/post/6893839274304700429)
      + [深入了解 Vue3 响应式原理](https://juejin.im/post/6893763807899271181)
   - 力扣刷题
      + [116. 填充每个节点的下一个右侧节点指针 II](https://leetcode-cn.com/problemset/all/)	
      + [118. 杨辉三角](https://leetcode-cn.com/problemset/all/)	
   - 源码阅读
      + [学习vue源码（4） 手写vm.$mount方法](https://juejin.im/post/6844904181438889991)
   - 项目进度(神树排行)  
      + 页面渲染
      + 跳转详情
      + 视频播放
      + 分类页面tab切换,切换对应数据

#### 11-10
   - 文章阅读
      + [🎉🎉一个基于 Vue 3 + Vant 3 的开源商城项目🎉🎉](https://juejin.im/post/6892783570016796679)
      + [掘金编辑器支持 Markdown 主题自定义啦！](https://juejin.im/post/6893338717562339335)
   - 力扣刷题
      + [100. 相同的树](https://leetcode-cn.com/problems/same-tree/)
      + [107. 二叉树的层次遍历 II](https://leetcode-cn.com/problems/binary-tree-level-order-traversal-ii/)
   - 源码阅读
      + [React源码阅读：概况](https://juejin.im/post/6844903640512086029)
   - 项目进度(神树排行)
      + 完成首页排版
      + 完成分类排版   
      
#### 11-9
   - 文章阅读
      + [Vue3.0系列——「vue3.0性能是如何变快的？」](https://juejin.im/post/6882566603762843655)
      + [Vue3.0简单认识](https://juejin.im/post/6844903967755862029)
      1

   - 力扣刷题
      + [103. 二叉树的锯齿形层次遍历](https://leetcode-cn.com/problems/binary-tree-zigzag-level-order-traversal/)
      + [112. 路径总和](https://leetcode-cn.com/problems/path-sum/)

   - 源码阅读
      + [vue3.0源码速读]https://juejin.im/post/6844904068964417550()

   - 项目进度(神树排名)   
      + 生成vue3框架
      + 完成页面排版
      + 请求接口
   - 遇到的问题
      + 接口请求报错   


### 宋超
#### 2020.11.23
- 文章阅读
    + [Vue3.0系列——「vue3.0性能是如何变快的？」](https://juejin.im/post/6882566603762843655)

- 源码阅读
    + [学习vue源码（4） 手写vm.$mount方法](https://juejin.im/post/6844904181438889991)

- leeCode刷题
    + [移除元素](https://leetcode-cn.com/problems/remove-element/)

- 项目进度
    + 使用umi脚手架搭建
    + 完成路由配置
    + 完成首页布局
    + 能跳转详情页面
    + 跳到详情页数据获取并渲染页面
#### 2020.11.20
- 文章阅读
    + [🎉🎉一个基于 Vue 3 + Vant 3 的开源商城项目🎉🎉](https://juejin.im/post/6892783570016796679)

- 源码阅读
    + [「JS」浅析 JS 原型链和继承](https://juejin.im/post/6893440135841775623)

- leeCode刷题
    + [两数相除](https://leetcode-cn.com/problems/divide-two-integers/)

- 项目进度
    + 使用umi脚手架搭建
    + 完成路由配置
    + 完成首页布局
    + 能跳转详情页面
    + 跳到详情页数据获取并渲染页面

#### 2020.11.19
- 文章阅读
    + [Vue 3.0 Ref-sugar 提案真的是自寻死路吗？](https://juejin.im/post/6894175515515551752)

- 源码阅读
    + [用原生实现Vue3](https://juejin.im/post/6893880467305529352?utm_source=gold_browser_extension)

- leeCode刷题
    + [对称二叉树](https://leetcode-cn.com/problemset/all/)

- 项目进度
    + 使用umi脚手架搭建
    + 完成路由配置
    + 完成首页布局
    + 能跳转详情页面，但是没拿到数据
#### 2020.11.18
- 文章阅读
    + [Vue3 模板编译优化](https://juejin.im/post/6893839274304700429)

- 源码阅读
    + [Umi开发Chrome扩展记录](https://juejin.im/post/6882623435396694023)

- leeCode刷题
    + [273. 整数转换英文表示](https://leetcode-cn.com/problems/integer-to-english-words/)

- 项目进度
    + 使用umi脚手架搭建
    + 完成路由配置
    + 
#### 2020.11.17
- 文章阅读
    + [vue2.0构建的一个文章阅读应用](https://blog.csdn.net/weixin_34218890/article/details/89140903)

- 源码阅读
    + [node](https://juejin.im/post/6844903778194292744)

- leeCode刷题
    + [ 串联所有单词的子串](https://leetcode-cn.com/problems/substring-with-concatenation-of-all-words/)

- 项目进度
    + 使用umi脚手架搭建
    + 完成todulist项目功能
    
      + 接口请求报错   
    
#### 2020.11.16
- 文章阅读
    + [Vue3.0简单认识](https://juejin.im/post/6844903967755862029)

- 源码阅读
    + [vue3.0源码速读]https://juejin.im/post/6844904068964417550()

- leeCode刷题
    + [删除排序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)

- 项目进度
    + 使用vue3.0版本下载脚手架
    + 完成路由配置
    + 请求到数据
    + 完成首页数据渲染
    + 完成分类页数据渲染，完成tab切换，完成点击跳转详情页
    + 完成中间数据点击，跳转排行详情页，显示每项的具体数据
    + 完成项目打包
    + 完成项目上线
#### 2020.11.13
- 文章阅读
    + [JavaScript 的数据类型及其检测](https://mp.weixin.qq.com/s/l4U4lVt_sz7lqT43aTuaTA)

- 源码阅读
    + [Vue源码阅读](https://www.jianshu.com/p/c4a42f2cde40)

- leeCode刷题
    + [2020前端面试问题及答案整理](https://www.jianshu.com/p/c26c9c1c73f0)

- 项目进度
    + 使用vue3.0版本下载脚手架
    + 完成路由配置
    + 请求到数据
    + 完成首页数据渲染
    + 完成分类页数据渲染，完成tab切换，完成点击跳转详情页
    + 完成中间数据点击，跳转排行详情页，显示每项的具体数据

#### 2020.11.12
- 文章阅读
    + [深入 JavaScript，从对象开始](https://juejin.im/entry/6844903476560920583)

- 源码阅读
    + [vue成长系列文章](https://github.com/xiaofuzi/deep-in-vue)

- leeCode刷题
    + [前端开发面试题集锦](https://www.runoob.com/w3cnote/front-end-development.html)

- 项目进度
    + 使用vue3.0版本下载脚手架
    + 完成路由配置
    + 请求到数据
    + 完成首页数据渲染
    + 完成分类页数据渲染，完成tab切换，完成点击跳转详情页
    + 完成中间数据点击，跳转排行详情页，显示每项的具体数据


#### 2020.11.11
- 文章阅读
    - [JavaScript 的数据类型及其检测](https://mp.weixin.qq.com/s/l4U4lVt_sz7lqT43aTuaTA)

  - 源码阅读
    - [web前端开发常用源码整理](https://blog.csdn.net/hefeng6500/article/details/80394742)

  - leeCode刷题
    - [web前端面试题合集](https://www.jianshu.com/p/e424e32f0943)
- 项目进度
    + 使用vue3.0版本下载脚手架
    + 完成路由配置
    + 请求到数据
    + 完成首页数据渲染
    + 完成中间数据点击，跳转排行详情页，显示每项的具体数据

#### 2020.11.10
- 文章阅读
  - [需要掌握的 28 个 JavaScript 技巧](https://juejin.im/post/6844903856489365518)

  - 源码阅读
    [vuex源码解析](http://tech.meituan.com/vuex-code-analysis.html)

  - leeCode刷题
    [前端面试题300道](https://blog.csdn.net/qq_22944825/article/details/78169321)
- 项目进度
    + 使用vue3.0版本下载脚手架
    + 完成路由配置
    + 请求到数据
    + 完成首页数据渲染
    
#### 2020.11.9
- 文章阅读
  - [JavaScript 执行机制](https://juejin.im/post/6844903512845860872)

  - 源码阅读
    [读 VuePress（四）插件机制](https://zhuanlan.zhihu.com/p/66435152)

  - leeCode刷题
    [vue 前端面试题整理](https://blog.csdn.net/u011269388/article/details/107763483)
- 项目进度
    - 使用vue3.0版本下载脚手架
    - 完成路由配置
    - 请求到数据
    - 完成一个死板swiper
    - 到的问题
    - vue3.0版本不熟练
### 任静
#### 2020.11.23
- 项目进度
  - 完成vconsole
  - 实现我的页面中的地址管理页面
- 文章阅读
  - [hooks](https://juejin.cn/post/6844903850659282957)
- leeCode刷题
  - set和map
  - promise
  - webpack
- 源码阅读
  - hooks源码阅读
#### 2020.11.19
- 项目进度
  - 封装获取购物车数据接口、购物车删除数据接口、购物车是否选中接口
  - 渲染购物车页面
  - 购物车全选反选
  - 购物车总价
  - 购物车数量加减

#### 2020.11.18
- 项目进度
  - 使用umi搭建脚手架
  - 配置八维严选路由
  - 完成登录功能
- 文章阅读
  - [react hook](https://juejin.im/post/6873795871708807175)
- leeCode刷题
  - v-if与v-show
  - vue组件传参
- 源码阅读
#### 2020.11.17
- 项目进度
  - 使用umi搭建脚手架
  - 完成todoList的添加、删除功能
- 文章阅读
  - [基于Umi的开发方案](https://juejin.im/post/6844903886029848589)
  - [umi基础知识](https://juejin.im/post/6844903557783633927)
- leeCode刷题
  - v-model的原理
  - 设计模式-发布订阅者模式，装饰器模式
- 源码阅读

#### 2020.11.16
- 项目进度
  - 封装懒加载模块
  - 神树排名添加图片懒加载效果
- 文章阅读
  - [vue3懒加载](https://www.cnblogs.com/boyyangD/articles/13660338.html)
  - [IntersectionObserver](https://www.ruanyifeng.com/blog/2016/11/intersectionobserver_api.html)
- leeCode刷题
  - vue组件中的data为什么必须是个函数而不是一个对象
  - vue 的响应原理
- 源码阅读
- 遇到的问题
  - 使用vue-lazyload不能实现图片懒加载
      - 解决方法：自己封装图片懒加载
 
#### 2020.11.13
- 项目进度
    - 配置神树排名热门搜索接口、搜索接口、搜索提示接口
    - 渲染热门搜索列表、搜索历史列表
    - 搜索历史数组去重
    - 渲染搜索后的搜索详情页面
    
#### 2020.11.12
- 项目进度
    - 完神树排名分类页面楼层效果
    - 完善渲染分类的详情页面
    - 完善详情页面下拉加载动画效果
    - 完善分类页面的搜索页面排版
- 文章阅读
  - [滚动监听 vue-scrollwatch](https://juejin.im/post/6844903559348256782)
  - [vue3 reactive 思路引导](https://juejin.im/post/6844904131216277511)
- leeCode刷题
  - Vue.js 组件的三个 API：(prop、event、slot)
  - vue实现数据双向绑定的原理
- 源码阅读

- 遇到的问题
  - 使用弹性盒布局子元素高度不能更改使用align-content: flex-start
#### 2020.11.11
- 项目进度
    - 完善神树排名分类页面排版
    - 渲染分类的详情页面
    - 详情页面下拉加载动画效果
    - 完成分类页面的搜索页面排版
- 文章阅读
  - [vue快速上手](https://juejin.im/post/6890153359966371854)
  - [vue3中rem适配](https://www.jianshu.com/p/1569664a6554)
- leeCode刷题
  - 严格模式的限制
  - es6中新增熟路类型set&map
  - 浅拷贝vs深拷贝
  - AMD &CMD
- 源码阅读

- 遇到的问题
  - 使用弹性盒布局子元素高度不能更改使用align-content: flex-start
#### 2020.11.10
- 项目进度
    - 神树排名分类页面排版
    - 封装神树排名分类api
    - 渲染神树排名分类页面
- 文章阅读
    - [vue3](https://juejin.im/post/6844903961628000263)
- leeCode刷题
    - async和await
    - Promise
    - ES6常用特性
- 源码阅读
#### 2020.11.9
- 项目进度
    - 配置神树排名路由
    - 配置神树排名接口
    - 获取神树排名首页数据
- 文章阅读
- leeCode刷题
    - cookie locationStorage  sessionStorage 
- 源码阅读
- leeCode刷题

### 蒋琳琳
#### 2020.11.23
- 项目进度
- 文章阅读
  - [JS面试：JS-Web-API](https://juejin.cn/post/6844903889305600014)
  - [Promise](https://juejin.cn/search?query=promise&type=all)
- leecode刷题
  - 删除排序数组中的重复项
  - 买卖股票的最佳时机 II
  - 旋转数组
- 源码阅读
  - hooks源码阅读
#### 2020.11.19
- 项目进度
  - 专题页面框架搭建
    - 页面的跳转
  - 专题的路由封装
    - 图片详情路由
    - 评价路由
    - 推荐路由
  - model封装
    - 获取图片详情
    - 获取评价
    - 获取推荐
  - 动态路由传参
    - [id]文件必须放在pages下面
- 文章阅读
  - [React 实现简易的图片拖动排序](https://juejin.cn/post/6896712416928169991?utm_source=gold_browser_extension)
  - [Vue响应式数据原理](https://juejin.cn/post/6896777102369456142?utm_source=gold_browser_extension)
  - [Vue3+TS，写一个逼格满满的项目](https://juejin.cn/post/6896748218076364814?utm_source=gold_browser_extension)
- leecode刷题
- 源码阅读
#### 2020.11.18
- 项目进度
  - 项目框架搭建
  - 路由搭建
  - 登录
    - 登录校验
    - token权限
- 文章阅读
  - [搭建 umi + qiankun + antd 的微前端平台](https://juejin.im/post/6869220236886245383)
  - [umi-request 网络请求之路](https://juejin.im/post/6844903982867939342)
- 源码阅读
  - lazyload
- leecode刷题
#### 2020.11.17
- 文章阅读
  - [react知识点](https://editor.csdn.net/md/?articleId=107340556)
****
  - 路由封装（实现重定向，路由守卫）

  - 生命周期
       ```js
      //创建期、实例期
      - constructor(props){}
      - componentWillMount(){}
      - render(){}
      - componentDidMount(){}

      //更新期、存在期
      - shouldComponentUpdata(){//组件状态（props）是否更新}
      - componentWillUpdata(){//组件更新前触发}
      - componentDidUpdata(){//组件更新完成触发}
      - componentWillUnmount(){}
       ```
  - redux仓库（基础）
#### 2020.11.16
- 遇到的问题
  - 使用弹性盒布局子元素高度不能更改使用align-content: flex-start
- 项目进度
    - 神树排名分类页面排版
    - 封装神树排名分类api
    - 渲染神树排名分类页面
- 文章阅读
    - [vue3](https://juejin.im/post/6844903961628000263)
- leeCode刷题
    - async和await
    - Promise
    - ES6常用特性
- 源码阅读
#### 2020.11.14
- 项目进度
  - 项目框架搭建
  - 路由搭建
  - 登录
    - 登录校验
    - token权限
- 文章阅读
  - [搭建 umi + qiankun + antd 的微前端平台](https://juejin.im/post/6869220236886245383)
  - [umi-request 网络请求之路](https://juejin.im/post/6844903982867939342)
- 源码阅读
  - lazyload
- leecode刷题
#### 2020.11.12
- 项目进度
  - 首页切换
  - 分类页面封装
    - 路由封装
    - vuex封装
  - 分类tab切换
  - 跳详情
  - 消息页面下拉刷新
- 源码阅读
  - 全局挂载loading
  - valueOf() 和 toString()
- leeCode刷题
- 文章阅读
  - [diff算法](https://www.cnblogs.com/wind-lanyan/p/9061684.html)
  - [用原生实现Vue3](https://juejin.im/post/6893880467305529352?utm_source=gold_browser_extension)
  - 整理[Web移动端适配方案](https://juejin.im/post/6894044091836563469?utm_source=gold_browser_extension)
#### 2020.11.11
- 项目进度
  - 首页排版
  - 首页接口的封装
  - 请求数据
    - vue3获取数据，通过computed获取
    - 获取事件通过dispath
  - 数据渲染
  - 路由跳转
- 源码阅读
  - Promise实现图片的依次加载和并行加载
  - 原生实现promise，实现依次调用
    - 思路一：利用异步，后置resove和reject的执行
    - 思路二：先保存resolve和reject执行时传递的参数，then方法里实际执行
- leeCode刷题
  -  transition闪屏问题
  -  fixed定位问题
  - 300ms延迟问题
  - 点击穿透问题
  - js不执行问题
- 文章阅读
  - [Vue3 模板编译优化](https://juejin.im/post/6893839274304700429?utm_source=gold_browser_extension#heading-3)
  - [深入了解 Vue3 响应式原理](https://juejin.im/post/6893763807899271181?utm_source=gold_browser_extension)
#### 2020.11.10
- 项目进度
   - 搭建基本框架
   - axios封装
   - api封装
   - vuex封装
   - 我的页面排版
   - 路由跳转
      - 路由跳转需要引入useRouter
   - 个人资料页面
- 源码阅读
   - Element.scrollIntoView()
   - Element.getBoundingClientRect()
- 文章阅读
   [vue3的变化](https://blog.csdn.net/qq_41107410/article/details/106132744)
   [Vue3+TS体验并开发+Vite浅析](https://juejin.im/post/6887478361623527431?utm_source=gold_browser_extension)
   [vue之web3.js开发之错误](https://blog.csdn.net/weixin_43343144/article/details/88578475)
- leeCode刷题
   - 禁止复制、选中文本
   - 修改input框的placeholder样式
   - 模拟实现PC上的hover效果
   - 启用硬件加速
   - 多倍屏1px像素问题和图片模糊问题
#### 2020.11.9
- 项目进度
    - 搭建vue3框架
    - 封装axios
    - 封装api
- 源码阅读
- 文章阅读
- leeCode刷题


