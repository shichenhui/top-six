### 倪建军
#### 2020.11.24

 2020.11.19umi，中文可发音为乌米，是一个可插拔的企业级 react 应用框架。umi 以路由为基础的，支持类 next.js 的约定式路由，以及各种进阶的路由功能，并以此进行功能扩展，比如支持路由级的按需加载。然后配以完善的插件体系，覆盖从源码到构建产物的每个生命周期，支持各种功能扩展和业务需求，目前内外部加起来已有 50+ 的插件。
umi 是蚂蚁金服的底层前端框架，已直接或间接地服务了 600+ 应用，包括 java、node、H5 无线、离线（Hybrid）应用、纯前端 assets 应用、CMS 应用等。他已经很好地服务了我们的内部用户，同时希望他也能服务好外部用户。
他包含以下特性：

📦 开箱即用，内置 react、react-router 等
🏈 类 next.js 且功能完备的路由约定，同时支持配置的路由方式
🎉 完善的插件体系，覆盖从源码到构建产物的每个生命周期
🚀 高性能，通过插件支持 PWA、以路由为单元的 code splitting 等
💈 支持静态页面导出，适配各种环境，比如中台业务、无线业务、egg、支付宝钱包、云凤蝶等
🚄 开发启动快，支持一键开启 dll 和 hard-source-webpack-plugin 等
🐠 一键兼容到 IE9，基于 umi-plugin-polyfills
🍁 完善的 TypeScript 支持，包括 d.ts 定义和 umi test
🌴 与 dva 数据流的深入融合，支持 duck directory、model 的自动加载、code splitting 等等

2.0 有什么改进？
轻内核 + 新手友好
umi@1 内置了很多优化方案，比如按需编译、按需加载、eslint、pwa、antd 校验等等，这些方案能提升开发体验和运行效率，但同时也提升了入门 umi 的门槛。
所以 umi@2 默认关掉了很多优化方案：

按需编译
按需加载
serviceWorker
antd
...

然后把这些功能改由插件来实现，按需开启，以保证 umi 内核的轻量。同时，默认构建只产生 index.html、umi.js 和 umi.css，对新手来说部署更友好。
全新的插件机制
umi@1 的插件机制有点过于强大，什么都能做，什么都能改。所以 umi@2 重构了插件机制，做了很多约束，明确什么能做，什么不能做，并提供了一套更友好的插件 API。
同时，这套插件机制已在内部得以验证，由超过 30 个插件构成的非常优秀的内部框架 Bigfish 正在服务于蚂蚁金服，包含埋点、后端接入、性能、服务接入、权限等等。
umi-plugin-react
umi@1 的插件比较散，使用时通常需要安装多个插件，升级和使用都比较麻烦，所以我们提供了 umi-plugin-react。umi-plugin-react 是插件集，类似 babel 里 preset 的概念。
目前有内置了 13 个插件，包含：

dva 整合
antd 整合
routes 修改
一键兼容 ie9
约定式的 i18n 方案
切换 react 到 preact 或其他类 react 库
路由级的按需加载，可定制按需的路由等级
通过 dll 提速
通过 hardSource 提速
pwa
启用高清方案
启用 fastClick
支持配置 title



请求库各式各样，没有统一。 每次新起应用都需要重复实现一套请求层逻辑，切换应用时需要重新学习请求库 API。
各应用接口设计不一致、混乱。 前后端同学每次需重新设计接口格式，前端同学在切换应用时需重新了解接口格式才能做业务开发。
接口文档维护各式各样。 有的在语雀（云端知识库）上，有的在 RAP （开源接口管理工具）上，有的靠阅读源码才能知道，无论是维护、mock 数据还是沟通都很浪费人力。
针对以上问题，我们提出了请求层治理，希望能通过统一请求库、规范请求接口设计规范、统一接口文档这三步，对请求链路的前中后三个阶段进行提效和规范， 从而减少开发者在接口设计、文档维护、请求层逻辑开发上花费的沟通和人力成本。其中，统一请求库作为底层技术支持，需要提前打好基地，为上层提供稳定、完善的功能支持，基于此，umi-request 应运而生。

umi-request
umi-request 是基于 fetch 封装的开源 http 请求库，旨在为开发者提供一个统一的 API 调用方式，同时简化使用方式，提供了请求层常用的功能：

URL 参数自动序列化
POST 数据提交方式简化
Response 返回处理简化
请求超时处理
请求缓存支持
GBK 编码处理
统一的错误处理方式
请求取消支持
Node 环境 http 请求
拦截器机制
洋葱中间件机制
与 fetch、axios 的异同？
特性	umi-request	fetch	axios
实现	fetch	浏览器原生支持	XMLHttpRequest
umi-request 底层抛弃了设计粗糙、不符合关注分离的 XMLHttpRequest，选择了更加语义化、基于标准 Promise 实现的 fetch（更多细节详见）；同时同构更方便，使用 isomorphic-fetch（目前已内置）；而基于各业务应用场景提取常见的请求能力并支持快速配置如 post 简化、前后缀、错误检查等。

上手便捷
安装
#### 2020.11.19
npm install --save umi-request
复制代码
执行  GET  请求

import request from "umi-request";
request
  .get("/api/v1/xxx?id=1")
  .then(function(response) {
    console.log(response);
  })
  .catch(function(error) {
    console.log(error);
  });
// 也可将 URL 的参数放到 options.params 里
request
  .get("/api/v1/xxx", {
    params: {
      id: 1
    }
  })
  .then(function(response) {
    console.log(response);
  })
  .catch(function(error) {
    console.log(error);
  });
复制代码
执行  POST  请求

import request from "umi-request";
request
  .post("/api/v1/user", {
    data: {
      name: "Mike"
    }
  })
  .then(function(response) {
    console.log(response);
  })
  .catch(function(error) {
    console.log(error);
  });
复制代码
实例化通用配置
请求一般都有一些通用的配置，我们不想在每个请求里去逐个添加，例如通用的前缀、后缀、头部信息、异常处理等等，那么可以通过 extend  来新建一个 umi-request 实例，从而减少重复的代码量：

import { extend } from "umi-request";

const request = extend({
  prefix: "/api/v1",
  suffix: ".json",
  timeout: 1000,
  headers: {
    "Content-Type": "multipart/form-data"
  },
  params: {
    token: "xxx" // 所有请求默认带上 token 参数
  },
  errorHandler: function(error) {
    /* 异常处理 */
  }
});

request
  .get("/user")
  .then(function(response) {
    console.log(response);
  })
  .catch(function(error) {
    console.log(error);
  });
复制代码
内置常见请求能力
fetch 本身并不提供请求超时、缓存、取消等能力，而在业务开发中却常常需要，因此 umi-request 对常见的请求能力进行封装内置，减少重复开发：

{
  // 'params' 是即将于请求一起发送的 URL 参数，参数会自动 encode 后添加到 URL 中
  // 类型需为 Object 对象或者 URLSearchParams 对象
  params: { id: 1 },

  // 'paramsSerializer' 开发者可通过该函数对 params 做序列化（注意：此时传入的 params 为合并了 extends 中 params 参数的对象，如果传入的是 URLSearchParams 对象会转化为 Object 对象
  paramsSerializer: function (params) {
    return Qs.stringify(params, { arrayFormat: 'brackets' })
  },

  // 'data' 作为请求主体被发送的数据
  // 适用于这些请求方法 'PUT', 'POST', 和 'PATCH'
  // 必须是以下类型之一：
  // - string, plain object, ArrayBuffer, ArrayBufferView, URLSearchParams
  // - 浏览器专属：FormData, File, Blob
  // - Node 专属： Stream
  data: { name: 'Mike' },

  // 'headers' 请求头
  headers: { 'Content-Type': 'multipart/form-data' },

  // 'timeout' 指定请求超时的毫秒数（0 表示无超时时间）
  // 如果请求超过了 'timeout' 时间，请求将被中断并抛出请求异常
  timeout: 1000,

  // 'prefix' 前缀，统一设置 url 前缀
  // ( e.g. request('/user/save', { prefix: '/api/v1' }) => request('/api/v1/user/save') )
  prefix: '',

  // 'suffix' 后缀，统一设置 url 后缀
  // ( e.g. request('/api/v1/user/save', { suffix: '.json'}) => request('/api/v1/user/save.json') )
  suffix: '',

  // 'credentials' 发送带凭据的请求
  // 为了让浏览器发送包含凭据的请求（即使是跨域源），需要设置 credentials: 'include'
  // 如果只想在请求URL与调用脚本位于同一起源处时发送凭据，请添加credentials: 'same-origin'
  // 要改为确保浏览器不在请求中包含凭据，请使用credentials: 'omit'
  credentials: 'same-origin', // default

  // 'useCache' 是否使用缓存，当值为 true 时，GET 请求在 ttl 毫秒内将被缓存，缓存策略唯一 key 为 url + params 组合
  useCache: false, // default

  // 'ttl' 缓存时长（毫秒）， 0 为不过期
  ttl: 60000,

  // 'maxCache' 最大缓存数， 0 为无限制
  maxCache: 0,

  // 'charset' 当服务端返回的数据编码类型为 gbk 时可使用该参数，umi-request 会按 gbk 编码做解析，避免得到乱码, 默认为 utf8
  // 当 parseResponse 值为 false 时该参数无效
  charset: 'gbk',

  // 'responseType': 如何解析返回的数据，当 parseResponse 值为 false 时该参数无效
  // 默认为 'json', 对返回结果进行 Response.text().then( d => JSON.parse(d) ) 解析
  // 其他(text, blob, arrayBuffer, formData), 做 Response[responseType]() 解析
  responseType: 'json', // default

  // 'errorHandler' 统一的异常处理，供开发者对请求发生的异常做统一处理，详细使用请参考下方的错误处理文档
  errorHandler: function(error) { /* 异常处理 */ },
}

#### 2020.11.18
关键词：

umi
qiankun
ant design pro
非动态注册子应用
非动态装载子应用（路由）


背景
银行项目，需要一个前端中台，承载各项目组开发的应用，本篇文章是在工作实践后所写，还是具有一定可借鉴性的，至于官网已经有相关内容还专门写这么一篇，还是因为在实践中官网中的部分不完善，导致搭建过程异常坎坷。

搭建主应用工程
主应用一般都是类后管平台，因此我使用 ant-deisng-pro 做主应用，使用以下命令创建：

参考：pro.ant.design/index-cn（页面底部）

yarn create umi main-app

ant-design-pro
pro v4
ts
simple
antd@4
复制代码

搭建子应用工程
使用以下命令创建：
yarn create @umijs/umi-app sub-app-1
复制代码

主应用

安装qiankun
yarn add qiankun
复制代码

安装@umijs/plugin-qiankun
yarn add @umijs/plugin-qiankun -D
复制代码

注册子应用

参考：
umijs.org/zh-CN/plugi…

在 config/config.ts 文件中，仿照以下代码进行添加：

子应用的工程名称是：sub-app-1
子应用的端口是：8001

{
  ...
  qiankun: {
    master: {
      apps: [
        {
          name: 'sub-app-1',
          entry: '//localhost:8001',
        },
      ],
    },
  },
}
复制代码

装载子应用（路由）
现在还差个指向子应用的路由。在 config/config.ts 文件中，仿照以下代码，在 routes 属性下进行添加：
{
  name: 'sub-app-1',
  icon: 'smile',
  path: '/sub-app-1',
  microApp: 'sub-app-1',
},
复制代码

示例文件
担心大家不会，此处还提供一个在已经配置好了的 config/config.ts 文件。config.ts

子应用

安装@umijs/plugin-qiankun
yarn add @umijs/plugin-qiankun -D
复制代码

注册刚安装的插件

参考：
umijs.org/zh-CN/plugi…

在 .umirc.ts 文件中添加以下代码：

qiankun: {
 slave: {}
}

添加后如下：
import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { path: '/', component: '@/pages/index' },
  ],
  qiankun: {
   slave: {}
  }
});


作者：blueju
来源：掘金
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
#### 2020.11.17
前置知识

干货-Chrome插件(扩展)开发全攻略
Umi官方文档
网页骨架屏自动生成方案（dps）

脚手架准备
1 安装 Scss
安装插件
yarn add @umijs/plugin-sass --dev
复制代码
另外在项目根目录typings.d.ts加上Scss模块说明
declare module '*.scss';
复制代码
2 设置hash路由，publicPath
为了我们的页面打开之后可以直接访问，需要设置
  history:{ type: 'hash' },
  publicPath:'./',
复制代码
3 设置Chrome扩展
这次写的是Chrome扩展，在开发调试的时候需要把js和css暴露在硬盘上；
设置:
  devServer:{
    writeToDisk:true,
  }
复制代码
package.json: 加上HTML=none不输出html文件
"build": "HTML=none umi build", 
复制代码
同时，在public目录新建index.html,这个index.html是在umi build产物copy的，这个html文件在构建的时候会放到dist目录下,去引用我们上面说的暴露在硬盘上的js和css文件
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no"
    />
    <link rel="stylesheet" href="./umi.css" />
    <script>
      window.routerBase = "/";
    </script>
    <script>
      //! umi version: 3.2.22
    </script>
  </head>
  <body>
    <div id="root"></div>

    <script src="./umi.js"></script>
  </body>
</html>

复制代码
然后，我们在public新建manifest.json声明这是一个Chrome扩展。
{
  "name": "FE chrome扩展",
	"manifest_version": 2,
  "version": "1.0.0",
  "description": "这是我的第一个chrome扩展",
  "icons": {
    "128": "img/icon.png"
  },
  "permissions": ["activeTab"],
  "content_scripts": [

  ],
  "browser_action": {
    "default_icon": "img/icon.png",
    "default_popup": "index.html"
  }
}
复制代码
4 目录参考
.                      
├── README.md                      
├── dist // chrome扩展目录,安装也是在这个文件夹    
│   ├── content_scripts                      
│   ├── img                      
│   ├── index.html                      
│   ├── manifest.json                      
│   ├── umi.css                      
│   └── umi.js                      
├── package.json                      
├── public // public目录的文件会原样copy到dist目录       
│   ├── content_scripts                      
│   ├── img                      
│   ├── index.html                      
│   └── manifest.json                      
├── src // src目录生成的文件会放到dist/umi.js和dist/umi.css文件给index.html引用    
│   ├── app.scss                      
│   ├── app.tsx                      
│   ├── components                      
│   ├── pages                      
│   └── utils                      
├── tsconfig.json                      
├── typings.d.ts                      
└── yarn.lock                                        
复制代码
DPS骨架屏扩展开发
dps骨架屏简单介绍
dps是基于 DOM 操作生成颜色块拼成骨架屏的方案，它通过单纯的 DOM 操作，遍历页面上的节点，根据制定的规则生成相应区域的颜色块，最终形成页面的骨架屏。
然后，通过Puppeteer 在无头浏览器运行evalDOM.js，生成骨架屏代码，注入到html页面中。没错，evalDOM.js是dps的核心。
由于Chromium在国内网络下载不方便，且体积较大，我们可以换种思路，借助Chrome扩展运行evalDOM.js,然后将生成的代码转换成css和tsx内容。
Chrome扩展开发
入口文件 manifest.json
{
  // ...
  "content_scripts": [
    {
      "matches": ["<all_urls>"],
      "js": ["content_scripts/evalDOM.js", "content_scripts/dps.js"],
      "run_at": "document_start"
    }
  ],
 // ...
}
复制代码
这里我们需要借助content-scripts(Chrome插件中向页面注入脚本的一种形式) 和 popup(点击browser_action或者page_action图标时打开的一个小窗口网页) 这两种能力。
我们向网页注入了evalDOM.js和dps.js,dps.js用于接收popup窗口传递过来的事件去调用evalDOM()方法生成骨架屏代码并返回内容。
dps.js
chrome.runtime.onMessage.addListener(function (request) {
 if (request.cmd == "dps") {
    // window.evalDOM方法由evalDOM.js提供
    window.evalDOM(request.value).then(skeletonHTML => { 
        document.body.innerHTML=skeletonHTML // 当前页面替换为骨架屏代码
        chrome.runtime.sendMessage(skeletonHTML); // 将骨架屏代码内容发送给popup.html
	}).catch(e => {
		console.error(e)
	})
 }
});
复制代码
popup窗口
popup窗口获取当前配置输入的内容,点击转换按钮将配置发送到dps.js
// 向content-script发送信息
$btn.onclick = ()=>{
    const value = parse($config.value); // AST解析字符串，生成带function字符的json
    sendMessageToContentScript( { cmd: "dps", value} );
}

// 监听来自content-script的消息
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
	const content = transform(request)
	document.querySelector("#style").value=content.style
	document.querySelector("#html").value=content.html
});
复制代码
全程代码都比较简单，原生几个js文件搞定~
如何向配置注入function函数
由于evalDOM接收参数有init()和includeElement(node, draw)两个函数，我们将配置写在textarea框里获取到的是一个字符串，如何解析带function的json字符串,JSON.parse()对格式有严格的要求，我们不想写{"fn":"function(){}"}这种代码，这时候，可以用AST大法解析。

esprima：解析AST代码的库
estraverse: 遍历更新AST
escodegen: 得到转译后的代码

/** json字符串转换成对象 */
function parse(str){
  const esprima = window.esprima // esprima库
  const traverse = window.traverse // estraverse库
  const escodegen = window.escodegen // escodegen 库

  const ast = esprima.parseScript('var a='+str); // 解析成AST
  const re = {};
  traverse(ast, {
    // 遍历
    enter: function (node) {
      if (node.type === "Property") {
        if (node.value.type === "Literal") {
          let key = node.key.name || node.key.value;
          let value = node.value.value;
          re[key] = value;
        }
        if (node.value.type === "FunctionExpression") {
          let key = node.key.name || node.key.value;
          let value = escodegen.generate(node.value); // 得到"function(){}"函数字符串
          // value = eval(`(function(){return ${value}})()`); // 可以通过eval转成函数
          re[key] = value;
        }
        this.skip();
      }
    }
  });
  return re
}




### 倪建军
#### 2020.11.16

import React, { Fragment } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import ScrollToTop from './components/ScrollToTop';
import Header from './components/Header';
import TimeLine from './pages/TimeLine';
import NotFound from './pages/404';

function App() {
  return (
    <Fragment>
      <BrowserRouter>
        <Header />
        <ScrollToTop>
          <Switch>
            <Redirect from="/" to="/timeline" exact />
            <Route path="/timeline" component={TimeLine}></Route>
            <Route component={NotFound}></Route>
          </Switch>
        </ScrollToTop>
      </BrowserRouter>
    </Fragment>
  );
}

export default App;
复制代码最后，通过在 App.js 中如此定义，即可定义对应的组件，并渲染对应页面和进行跳转。
四 简介

返回目录

下面我们拿一些常用的进行介绍：
import { 
  BrowserRouter,
  HashRouter,
  Redirect,
  Route,
  NavLink,
  Link,
  MemoryRouter,
  Switch,
  withRouter
} from "react-router-dom";
复制代码
<BrowserRouter>：路由组件包裹层。<Route> 和 <Link> 的包裹层。
<HashRouter>：路由组件包裹层。相对于 <BrowserRouter> 来说，更适合静态文件的服务器。
<Redirect>：路由重定向。渲染 <Redirect> 将使导航到一个新的地址。
<Route>：路由。定义一个路由页面，用来匹配对应的组件（Component）和路由路径。
<NavLink>：活跃链接。当 URL 中的路径等于该路由定义的路径时，该标签可以呈现它定义的 activeClassName。
<Link>：链接。用来跳转到 <Route> 对应的路由（Component） 中。
<MemoryRouter>：暂未使用。<Router> 能在内存中保存 URL 的历史记录。很适合在测试环境和非浏览器环境中使用，例如 React Native。
<Switch>：路由分组。渲染与该地址匹配的第一个子节点 <Route>或者 <Redirect>。可以利用 <Switch> 做分组。
<withRouter>：路由组合。通过 <withRouter> 高阶组件访问 history 对象的属性和最近的 <Route> 的 match。或者利用它来结合 Redux。



### 倪建军
#### 2020.11.12
`import $observable from 'symbol-observable`'

`import ActionTypes from './utils/actionTypes'
import isPlainObject from './utils/isPlainObject'`

`export default function createStore(reducer, preloadedState, enhancer) {

   判断enhancer是否为function，如果是的话
   return enhancer(createStore)(reducer, preloadedState)
   enhancer(增强器) 就是 applyMiddleware(/*...中间件，增强dispatch...*/)
  if (typeof enhancer !== 'undefined') {
    if (typeof enhancer !== 'function') {
      throw new Error('Expected the enhancer to be a function.')
    }

    return enhancer(createStore)(reducer, preloadedState)
  }`


  let currentReducer = reducer
  let currentState = preloadedState
  let currentListeners = []
  let nextListeners = currentListeners
  let isDispatching = false

  function ensureCanMutateNextListeners() {
    if (nextListeners === currentListeners) {
      nextListeners = currentListeners.slice()
    }
  }

  - 返回最新的状态值
  function getState() {
    if (isDispatching) {
      throw new Error(
        'You may not call store.getState() while the reducer is executing. ' +
          'The reducer has already received the state as an argument. ' +
          'Pass it down from the top reducer instead of reading it from the store.'
      )
    }

    return currentState
  }

   顾名思义，就是订阅，传入监听者函数，然后存储到nextListeners容器中，并且返回
   一个取消订阅的函数，用于卸载订阅函数。使用了闭包的方式，找到对应的listener的
   index值，然后将它从nextListeners容器中剔除
  function subscribe(listener) {
    if (typeof listener !== 'function') {
      throw new Error('Expected the listener to be a function.')
    }

    if (isDispatching) {
      throw new Error(
        'You may not call store.subscribe() while the reducer is executing. ' +
          'If you would like to be notified after the store has been updated, subscribe from a ' +
          'component and invoke store.getState() in the callback to access the latest state. ' +
          'See https://redux.js.org/api-reference/store#subscribelistener for more details.'
      )
    }

    let isSubscribed = true

    ensureCanMutateNextListeners()
    nextListeners.push(listener)

    return function unsubscribe() {
      if (!isSubscribed) {
        return
      }

      if (isDispatching) {
        throw new Error(
          'You may not unsubscribe from a store listener while the reducer is executing. ' +
            'See https://redux.js.org/api-reference/store#subscribelistener for more details.'
        )
      }

      isSubscribed = false

      ensureCanMutateNextListeners()
      const index = nextListeners.indexOf(listener)
      nextListeners.splice(index, 1)
      currentListeners = null
    }
  }
  
  /*
    每一次dispatch一个action的时候，都会执行currentReducer
    而currentReducer就是最初传递进来的reducer的集合
    执行完currentReduce，就会返回最新的状态，然后将状态赋值给currentState
    而currentState就是getState方法返回所需要的值也就是最新的状态
    
    同时循环遍历listeners，执行其中的监听方法。
    listeners的数据来自于subscribe，subscribe就是订阅。
  */
  function dispatch(action) {
    if (!isPlainObject(action)) {
      throw new Error(
        'Actions must be plain objects. ' +
          'Use custom middleware for async actions.'
      )
    }

    if (typeof action.type === 'undefined') {
      throw new Error(
        'Actions may not have an undefined "type" property. ' +
          'Have you misspelled a constant?'
      )
    }

    if (isDispatching) {
      throw new Error('Reducers may not dispatch actions.')
    }

    try {
      isDispatching = true
      currentState = currentReducer(currentState, action)
    } finally {
      isDispatching = false
    }

    const listeners = (currentListeners = nextListeners)
    for (let i = 0; i < listeners.length; i++) {
      const listener = listeners[i]
      listener()
    }

    return action
  }

  function replaceReducer(nextReducer) {
    if (typeof nextReducer !== 'function') {
      throw new Error('Expected the nextReducer to be a function.')
    }

    currentReducer = nextReducer

    dispatch({ type: ActionTypes.REPLACE })
  }

  function observable() {
    const outerSubscribe = subscribe
    return {
      subscribe(observer) {
        if (typeof observer !== 'object' || observer === null) {
          throw new TypeError('Expected the observer to be an object.')
        }

        function observeState() {
          if (observer.next) {
            observer.next(getState())
          }
        }

        observeState()
        const unsubscribe = outerSubscribe(observeState)
        return { unsubscribe }
      },

      [$$observable]() {
        return this
      }
    }
  }

  dispatch({ type: ActionTypes.INIT })

  return {
    dispatch,
    subscribe,
    getState,
    replaceReducer,
    [$$observable]: observable
  }
}

### 倪建军
#### 2020.11.11

tabBar": {

    "color": "#dddddd",

    "selectedColor": "#d92121",

    "borderStyle": "white",

    "backgroundColor": "#fff",

    "list": [{

      "pagePath": "pages/index",

      "iconPath": "images/main.png",

      "selectedIconPath": "images/main-s.png",

      "text": "主页"

    },{

      "pagePath": "pages/layout/hot",

      "iconPath": "images/hot.png",

      "selectedIconPath": "images/hot-s.png",

      "text": "最热"

    },{

      "pagePath": "pages/layout/new",

      "iconPath": "images/new.png",

      "selectedIconPath": "images/new-s.png",

      "text": "最新"

    }]
    
### 倪建军
#### 2020.11.10

    // 创建新的链表节点
    const newNode = new Node(data);

    // 追加链表节点；共用一下几种情况：
    // 1. 空链表追加到头部
    // 2. 链表有数据追加到头部
    // 3. 追加到尾部
    // 4. 0----this.length 之间的追加
    if (position === 0) {
      // 追加到头部
      if (this.head === null) {
        // 链表无数据添加到头部
        this.head = newNode;
        this.tail = newNode;
      } else {
        // 链表有数据添加到头部
        newNode.next = this.head;
        this.head.previous = newNode;
        this.head = newNode;
      }
    } else if (position === this.length) {
      // 在链表尾部插入节点
      this.tail.next = newNode;
      newNode.previous = this.tail;
      this.tail = newNode;
    } else {
      // 0 --- this.length 中间插入

      // 准备变量
      let targetIndex = 0; // 代表节点位置的索引
      let currentNode = this.head;
      let previousNode = null; // 代表上一个节点

      // 循环查找节点
      while (targetIndex++ < position) {
        previousNode = currentNode;
        currentNode = currentNode.next;
      }

      // 插入
      previousNode.next = newNode;
      newNode.previous = previousNode;

      newNode.next = currentNode;
      currentNode.previous = newNode;
    }

    // 增加长度
    this.length++;
    return true;
  }



