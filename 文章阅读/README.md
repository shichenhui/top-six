### 倪建军
#### 2020.11.24
aHooks 的代码片段整理，这篇是整理了aHooks中的一部分方法，为了方便自己快速查阅和使用。此文中的方法，一部分是自己比较常用的，一部分是我个人认为比较实用，关于剩下没有加入的方法，可能是我本人还没有发现它的优点，大家可以移步到官网查阅哦！(下面的方法中，部分代码没有在项目中用过的方法，是直接copy的官网，关于自己用过的会有添加注释，如果没有，大家以官网的解释为主哦！)

!!!!! 原来的Umi hooks 项目整合重新升级，现在使用的Umi hooks 有了新的替代品aHooks，原来使用Umi Hooks 的同学可以跟着升级，基本上就是引入的包名换了一下，其他的有细微更改，下文中我也跟着做了些更改，大家使用的时候注意就可以哦！
// umi hooks 的引入方式
import { useDebounce } from '@umijs/hooks';

// ahooks 的引入方式
import { useRequest } from 'ahooks';
复制代码如果看到这篇文的你，恰好也是也在用react 的umi库的小伙伴，我是大力推荐使用umi Hooks，真香！
Umi Hooks
aHooks

一、Async 之 useRequest 异步数据请求
1.自动请求
  const { data } = useRequest(
    async () => {
      const response = await getList();
      return response;
    }
  );

复制代码2.手动请求：manual、run
 const { data: list, run: listRequest } = useRequest(
    async () => {
      const response = await getList();
      return response;
    },
    {
      manual: true,
    },
  );

  useEffect(() => {
    if('某些条件'){
    listRequest();
    }
  }, []);

复制代码3. 手动暂停请求：cancel
 const { data, run, cancel } = useRequest(
    async () => {
      const response = await getList();
      return response;
    },
    {
      manual: true,
    },
  );

<Button.Group>
    <Button onClick={run}>开始请求</Button>
    <Button onClick={cancel}>停止请求</Button>
</Button.Group>


#### 2020.11.19
  面对多端、多设备应用，umi-request 不仅支持 browser http 请求，也同时满足 node 环境、自定义内核请求等能力。
支持 node 环境发送 http 请求
基于  isomorphic-fetch 实现对 node 环境的请求支持：
const umi = require("umi-request");
const extendRequest = umi.extend({ timeout: 10000 });

extendRequest("/api/user")
  .then(res => {
    console.log(res);
  })
  .catch(err => {
    console.log(err);
  });
复制代码支持自定义内核请求能力
移动端应用一般都会有自己的请求协议如 RPC 请求，前端会通过 SDK 去调用客户端请求 API，umi-request 支持开发者自己封装请求能力，例子：
// service/some.js
import request from "umi-request";
// 自定义请求内核中间件
function SDKRequest(ctx, next) {
  const { req } = ctx;
  const { url, options } = req;
  const { __umiRequestCoreType__ = "normal" } = options;

  if (__umiRequestCoreType__.toLowerCase() !== "SDKRequest") {
    return next();
  }

  return Promise.resolve()
    .then(() => {
      return SDK.request(url, options); // 假设已经引入了 SDK 并且能通过 SDK 发起对应请求
    })
    .then(result => {
      ctx.res = result; // 将结果注入到 ctx 的 res 里
      return next();
    });
}

request.use(SDKRequest, { core: true }); // 引入内核中间件

export async function queryUser() {
  return request("/api/sdk/request", {
    __umiRequestCoreType__: "SDKRequest", // 声明使用 SDKRequest 来发起请求
    data: []
  });
}


#### 2020.11.18

在做中台业务应用开发的过程中，我们发现在请求链路上存在以下问题：

请求库各式各样，没有统一。 每次新起应用都需要重复实现一套请求层逻辑，切换应用时需要重新学习请求库 API。
各应用接口设计不一致、混乱。 前后端同学每次需重新设计接口格式，前端同学在切换应用时需重新了解接口格式才能做业务开发。
接口文档维护各式各样。 有的在语雀（云端知识库）上，有的在 RAP （开源接口管理工具）上，有的靠阅读源码才能知道，无论是维护、mock 数据还是沟通都很浪费人力。

针对以上问题，我们提出了请求层治理，希望能通过统一请求库、规范请求接口设计规范、统一接口文档这三步，对请求链路的前中后三个阶段进行提效和规范， 从而减少开发者在接口设计、文档维护、请求层逻辑开发上花费的沟通和人力成本。其中，统一请求库作为底层技术支持，需要提前打好基地，为上层提供稳定、完善的功能支持，基于此，umi-request 应运而生。
umi-request
umi-request 是基于 fetch 封装的开源 http 请求库，旨在为开发者提供一个统一的 API 调用方式，同时简化使用方式，提供了请求层常用的功能：

URL 参数自动序列化
POST 数据提交方式简化
Response 返回处理简化
请求超时处理
请求缓存支持
GBK 编码处理
统一的错误处理方式
请求取消支持
Node 环境 http 请求
拦截器机制
洋葱中间件机制

与 fetch、axios 的异同？



特性
umi-request
fetch
axios




实现
fetch
浏览器原生支持
XMLHttpRequest





umi-request 底层抛弃了设计粗糙、不符合关注分离的 XMLHttpRequest，选择了更加语义化、基于标准 Promise 实现的 fetch（更多细节详见）；同时同构更方便，使用 isomorphic-fetch（目前已内置）；而基于各业务应用场景提取常见的请求能力并支持快速配置如 post 简化、前后缀、错误检查等。
上手便捷
安装
npm install --save umi-request
复制代码执行  GET  请求
import request from "umi-request";
request
  .get("/api/v1/xxx?id=1")
  .then(function(response) {
    console.log(response);
  })
  .catch(function(error) {
    console.log(error);
  });
// 也可将 URL 的参数放到 options.params 里
request
  .get("/api/v1/xxx", {
    params: {
      id: 1
    }
  })
  .then(function(response) {
    console.log(response);
  })
  .catch(function(error) {
    console.log(error);
  });
复制代码执行  POST  请求
import request from "umi-request";
request
  .post("/api/v1/user", {
    data: {
      name: "Mike"
    }
  })
  .then(function(response) {
    console.log(response);
  })
  .catch(function(error) {
    console.log(error);
  });



#### 2020.11.17
基于Umi的开发方案
Umi是阿里的一款基于React的企业级应用框架。本文将会从3个方面介绍下基于Umi的开发方案：

umi是什么？
怎么使用umi？
umi是如何实现的？

umi是什么
umi是一款可插拔的企业级react应用框架，支持约定式路由以及各种进阶路由功能，并以此进行功能扩展，拥有完善的插件体系，覆盖从源码到构建产物的每个生命周期，支持各种功能扩展和业务需求。
它有以下特性：

开箱即用，内置react，react-router等
约定式路由，同时支持可配置路由
完善的插件体系
支持typescript
支持dva数据方案

一个umi工程的生命周期如下，它包含源码到上线的整个流程，umi 首先会加载用户的配置和插件，然后基于配置或者目录，生成一份路由配置，再基于此路由配置，把 JS/CSS 源码和 HTML 完整地串联起来。用户配置的参数和插件会影响流程里的每个环节。

使用
创建一个umi项目
创建一个umi项目可通过2种方式，手工创建和脚手架创建。
手工创建
第一步，创建相关文件夹：
mkdir umi_app && cd umi_app
npm init
mkdir pages
复制代码第二步，增加npm script:
"scripts": {
    "start": "umi dev",
    "build": "umi build”,
}
复制代码第三步，增加依赖：
"devDependencies": {
    "umi": "^2.6.3"
}
复制代码第四步，在pages目录下，增加新模块。
最后，使用npm run start即可运行该项目。
脚手架创建
umi提供了脚手架工具create-umi来加快umi工程的创建。
mkdir umi_app && cd umi_app
create-umi
npm i
复制代码最后，使用npm run start即可运行项目，在localhost:8000访问该项目。
业务开发
创建出来的项目目录结构如下：
.
├── dist/                          // 默认的 build 输出目录
├── mock/                          // mock 文件所在目录，基于 express
├── config/
    ├── config.js                  // umi 配置，同 .umirc.js，二选一
└── src/                           // 源码目录，可选
    ├── layouts/index.js           // 全局布局
    ├── pages/                     // 页面目录，里面的文件即路由
        ├── .umi/                  // dev 临时目录，需添加到 .gitignore
        ├── .umi-production/       // build 临时目录，会自动删除
        ├── document.ejs           // HTML 模板
        ├── 404.js                 // 404 页面
        ├── page1.js               // 页面 1，任意命名，导出 react 组件
        ├── page1.test.js          // 用例文件，umi test 会匹配所有 .test.js 和 .e2e.js 结尾的文件
        └── page2.js               // 页面 2，任意命名
    ├── global.css                 // 约定的全局样式文件，自动引入，也可以用 global.less
    ├── global.js                  // 可以在这里加入 polyfill
    ├── app.js                     // 运行时配置文件
├── .umirc.js                      // umi 配置，同 config/config.js，二选一
├── .env                           // 环境变量
└── package.json
复制代码.umi目录是umi dev生成的临时目录，默认包含 umi.js 和 router.js。.umi-production是在umi build生成的临时目录。
通过命令行umi g page users生成users页面，在localhost:8000/users即可访问该页面。
同时，也可以使用dva作为状态管理工具配合umi进行开发，可参考umi + dva，完成用户管理的 CURD 应用。
使用插件
基于umi的插件机制，你可以获得扩展项目的编译时和运行时的能力。通过插件支持的功能也会变得更强大，我们针对功能的需要可以去使用修改代码打包配置，修改启动代码，约定目录结构，修改 HTML 等更丰富接口。插件可以是一个 npm 包，也可以是路径直接引向一个 JS 的模块。用户通过配置 plugins 来使用插件。如下所示：
// .umirc.js
export default {
  plugins: [
    [
      'umi-plugin-dva',
      {
        immer: true,
      },
    ],
    [
      './src/plugins/customPlugin.js',
      {
        // plugin config
      },
    ],
  ],
};


### 倪建军
#### 2020.11.16
用 react-router 也用了比较久了，对他的内部工作方式却只是了解皮毛，而且大部分还是通过别人的博客。最近两周打算自己探究一下他的实现。
注意！因为我只使用过 v3 版本的 react-router，因为对他的使用方式比较熟悉，所以这次解析也是基于这个版本。
文章目录：

react-router 工作模式简化流程
内部具体实现
Link 组件的实现方式 以及 *History.push 的实现方式
为什么要这样实现？有什么别的方式？做个比较？

react-router 工作模式简化流程
聊到这个话题就离不开前端路由。关于前端路由的一些演变过程和现有的方式可以看这篇文章。前端路由的重点就是不刷新页面，现有的解决方案有 hashChange 和 popState 两种。
React 提供API也是围绕这两种方式。 共同点都是发布订阅的模式，让浏览器事件触发的时候自己添加的 listener 被调用。Router 组件包裹着 Route 组件，Route 组件负责描述每条路由的展示组件和匹配的路径。这样 Router 组件实际上会格式化出一个映射的路由表。
然而这是在页面路由更新的时候，最开始进入页面的时候怎么办呢？其实刚进入页面的时候也会进行一次匹配，详细分析见下一部分。
内部具体实现
首先解答上面的遗留问题，刚进入页面的状态如何带入？这个问题我们可以和 "Router 组件是在什么时候添加的事件监听"放在一起解答。

复制代码Router 组件在 willMount 生命周期添加了 listener，而添加 listener 本身就会触发一次匹配路由展示的过程。匹配的过程有 match 方法，用于各种嵌套路由的匹配。
但是注意，如果使用的是 browserHistory，这种路由方式一般是/a/b 这种方式，可能需要后端同学的配合。
封装 history ——transitionManager
在上面的代码中，我们会注意到添加监听器的 listen方法来自于 transitionManager 这个生成之后被赋值到 this.router 实例的属性。实际上 react-router 的事件监听过程是用 transitionManager 套了 history 这个库，抹平各种前端路由方式的调用差异。history库本身暴露了一些API 比如监听、取消监听、跳转等一系列方法。有基于咱们刚才提到的 hash 和 state 两种方式。我们传给 Router 组件 history 属性的值其实就是他的实例。（拿hashHistory 举栗，下面的文件是 reate-router export 的 hashHistory 的来源，也就是我们用的 hashHistory 的来源）。
//来源：modules/hashHistory.js
import createHashHistory from 'history/lib/createHashHistory'
import createRouterHistory from './createRouterHistory'
export default createRouterHistory(createHashHistory)
复制代码而 transitionManager 做的事情是针对当前的 router 实例和开发者指定的 history 对象，对 history 库给的 API 做一次二次封装，加上修改路由状态等等操作。然后开发者拿着 transitionManager 封装之后暴露出的 listen 等方法操作路由。
  createTransitionManager() {//来源：modules/Router.js
    const { matchContext } = this.props
    if (matchContext) {
      return matchContext.transitionManager
    }

    const { history } = this.props
    const { routes, children } = this.props

    invariant(
      history.getCurrentLocation,
      'You have provided a history object created with history v4.x or v2.x ' +
        'and earlier. This version of React Router is only compatible with v3 ' +
        'history objects. Please change to history v3.x.'
    )

    return createTransitionManager(//注意这个createTransitionManager才是
      history,
      createRoutes(routes || children)
    )
  },
复制代码渲染部分
渲染过程不是放在 Route 组件中负责渲染，而是把状态都放在 Router 中保存，详细可见第一部分的代码添加 listener 的部分。
     assignRouterState(this.router, state)
     this.setState(state, this.props.onUpdate)
复制代码而 Router 组件的 render 是这样写的：
    const { location, routes, params, components } = this.state
    const { createElement, render, ...props } = this.props

    return render({
      ...props,
      router: this.router,
      location,
      routes,
      params,
      components,
      createElement
    })
复制代码而 props 的值是当前 Router 组件的状态，他现在要展示的组件，对应的地址，当前跳转携带的参数 params 等等。下面是调用 render 的部分。


### 倪建军
#### 2020.11.12
<user-card data-open="true"></user-card>

//javascript:
class Learn extends HTMLElement{
    constructor(props) {
        super(props);
        console.log(this.dataset);
        this.innerHTML = '这是我自定义的元素';
        this.style.border = '1px solid #899';
        this.style.borderRadius = '3px';
        this.style.padding = '4px';
    }
}
window.customElements.define('user-card',Learn);

解析：通过window.customElements方法可以创建自定义元素，里面的define方法就是用来指定自定义元素的名称，以及自定义元素对应的类。
这里有一个细节，自定义元素中间一定要用中划线隔开，不然是无效的。
这时候在这个类里面就可以定义元素里的所有内容了，这和Vue里面的组件已经比较类似了，有了这个基础之后我们再往里面去进行拓展就可以实现组件了。
必备知识2，Proxy
这家伙估计大家都知道，Vue3数据响应的核心，Vue2用的是Object.defineProperty;
很强大，很好用，先来个简单的代码：
let obj = {
    a:2938,
    b:'siduhis',
    item:'name'
}

obj = new Proxy(obj,{
    set(target, p, value, receiver) {
        console.log('监听到',p,'被修改了,由原来的：',target[p],'改成了：',value);
    }
});

document.onclick = ()=>{
    obj.item = 'newValue';
}


### 倪建军
#### 2020.11.11


- 微信小程序入门一文章列表
   - 第一步，先搭好骨架，创建一个list文件夹，用来存放文章列表相关的内容，官方文档中也强调过：“为了方便开发者减少配置项，我们规定描述页面的这四个文件（js，wxml，wxss，json）必须具有相同的路径与文件名。”

（打开项目硬盘目录，然后把index文件夹下的内容copy到list下面，文件名好像会自动修改）

第二步，在 app.json 中配置 pages ，"pages/list/list" 不需要添加文件后缀名（模仿index来即可）。

同时也可以添加底部tabBar （官方文档：https://mp.weixin.qq.com/debug/wxadoc/dev/framework/config.html）
"tabBar": {
    "list": [{
      "pagePath": "pages/list/list",
      "text": "文章列表"
    }, {
      "pagePath": "pages/logs/logs",
      "text": "日志"
    }]
  },

  
配置好后，便可以在list.wxml文件中写代码：

<!--pages/list/list.wxml-->
<text>这是文章列表页面</text>
<!--用name 定义模版-->
<template name="msgTemp">

1. scaleToFill : 图片全部填充显示，可能导致变形
2. aspectFit ： 图片全部显示，以最长边为准
3. aspectFill ： 图片全部显示，以最短边为准
4. widthFix ： 宽不变，全部显示图片

<view>
    <image src="{{src}}" mode="scaleToFill"></image>
</view>
<view>
    <text>{{title}}</text>
    <text>{{time}}</text>
</view>
</template>
 
<view wx:for="{{msgList}}">
    <!--用is 使用模版-->
    <template is="msgTemp" data="{{...item}}"/>
</view>
最后一步是在list.js中定义数据
在Page 下 data中添加 （图片路径是相对路径，根据自己的情况修改）

data:{
    msgList:[
      {id:1,title:"标题一",time:"2017-3-5 23:01:59",src:"../../images/wechatHL.png"},
      {id:2,title:"标题二",time:"2017-3-5 23:02:59",src:"../../images/wechatHL.png"},
      {id:3,title:"标题三",time:"2017-3-5 23:03:59",src:"../../images/wechatHL.png"},
      {id:4,title:"标题四",time:"2017-3-5 23:04:59",src:"../../images/wechatHL.png"}
    ]

 
 ### 倪建军
 #### 2020.11.10
 - mvvm数据响应实现
 - 当前vue、react等框架流行。无论是vue、还是react框架大家最初的设计思路都是类似的。都是以数据驱动视图，数据优先。希望能够通过框架减少开发人员直接操作节点，让开发人员能够把更多的精力放在业务上而不是过多的放在操作节点上。另一方面，框架会通过虚拟dom及diff算法提高页面性能。这其中需要数据优先最根本的思路就是实现数据响应式。so，本次来看下如何基于原生实现数据响应式。
vue中的数据响应
vue中会根据数据将数据通过大胡子语法及指令渲染到视图上，这里我们以大胡子语法为例。如下：
<div id="app">
        {{message}}
</div>
let vm = new Vue({
    el:"#app",
    data:{
        message:"测试数据"
    }
})
setTimeout(()=>{
    vm.message = "修改的数据";
},1000)
实现数据初次渲染
根据vue调用方式。定义Vue类来实现各种功能。将初次渲染过程定义成编译compile函数渲染视图。通过传入的配置以及操作dom来实现渲染。大概思路是通过正则查找html 里 #app 作用域内的表达式，然后查找数据做对应的替换即可。具体实现如下：
class Vue {
    constructor(options) {
        this.opts = options;
        this.compile();
    }
    compile() {
        let ele = document.querySelector(this.opts.el);
        // 获取所有子节点
        let childNodes = ele.childNodes;
        childNodes.forEach(node => {
            if (node.nodeType === 3) {
                // 找到所有的文本节点
                let nodeContent = node.textContent;
                // 匹配“{{}}”
                let reg = /\{\{\s*([^\{\}\s]+)\s*\}\}/g;
                if (reg.test(nodeContent)) {
                    let $1 = RegExp.$1;
                    // 查找数据替换 “{{}}”
                    node.textContent = node.textContent.replace(reg, this.opts.data[$1]);
                }
            }
        })
    }
}
如上完成了初次渲染，将message数据渲染到了视图上。但是会返现并没对深层次的dom结构做处理也就是如下情况：
  <div id="app">
        1{{ message }}2
        <div>
            hello , {{ message }}
        </div>
    </div>

发现结果并没有达到预期。so，需要改下代码，让节点可以深层次查找就可以了。代码如下：
   compile() {
        let ele = document.querySelector(this.opts.el);
        this.compileNodes(ele);
    }
    compileNodes(ele) {
        // 获取所有子节点
        let childNodes = ele.childNodes;
        childNodes.forEach(node => {
            if (node.nodeType === 3) {
                // 找到所有的文本节点
                let nodeContent = node.textContent;
                // 匹配“{{}}”
                let reg = /\{\{\s*([^\{\}\s]+)\s*\}\}/g;
                if (reg.test(nodeContent)) {
                    let $1 = RegExp.$1;
                    // 查找数据替换 “{{}}”
                    node.textContent = node.textContent.replace(reg, this.opts.data[$1]);
                }
            } else if (node.nodeType === 1) {
                if (node.childNodes.length > 0) {
                    this.compileNodes(node);
                }
            }
        })
    }
数据劫持
回过头来看下上面说的第二个问题：当message数据改变的时候视图上渲染的message数据同时也会做出响应。如何实现数据响应式？简而言之就是数据变动影响视图变动？再将问题拆分下 1. 如何知道数据变动了？  2.如何根据数据变动来更改视图？

如何知道数据变动了？
这里就需要用到数据拦截了，或者叫数据观察。把会变动的data数据观察起来。当他变动的时候我们可以做后续的渲染事情。如何拦截数据呢 ？vue2里采取的是definePrototype。

let obj = {
   myname:"张三"
}
Object.defineProperty(obj,'myname',{
   configurable:true,
   enumerable:true,
   get(){
       console.log("get.")
       return "张三";
   },
   set(newValue){
       console.log("set")
       console.log(newValue);
   }
})
console.log(obj);


还有没有其他方式达到数据劫持的效果呢？ES6中出现了Proxy 代理对象同样也可以达到类似劫持数据的功能。如下代码：
let obj = {
    myname:"张三"
}
let newObj = new Proxy(obj,{
    get(target,key){
        console.log("get...")
        return "张三"
    },
    set(target,name,newValue){
        console.log("set...");
       return Reflect.set(target,name,newValue);
    }
})

observe(data){
        let keys = Object.keys(data);
        keys.forEach(key=>{
            let value = data[key];
            Object.defineProperty(data,key,{
                configurable:true,
                enumerable:true,
                get(){
                    return value;
                },
                set(newValue){
                    value = newValue;
                }
            });
        })
 }
