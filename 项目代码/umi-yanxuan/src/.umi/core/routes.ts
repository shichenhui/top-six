// @ts-nocheck
import React from 'react';
import { ApplyPluginsType } from 'C:/Users/宋超/Desktop/umi-market/node_modules/@umijs/runtime';
import * as umiExports from './umiExports';
import { plugin } from './plugin';

export function getRoutes() {
  const routes = [
  {
    "path": "/brandDetail/:id",
    "exact": true,
    "component": require('@/pages/brandDetail/[id].tsx').default
  },
  {
    "path": "/detail/:id",
    "exact": true,
    "component": require('@/pages/detail/[id].tsx').default
  },
  {
    "path": "/",
    "exact": true,
    "component": require('@/pages/index.tsx').default
  },
  {
    "path": "/login",
    "exact": true,
    "component": require('@/pages/login.tsx').default
  },
  {
    "path": "/shopDetail/:id",
    "exact": true,
    "component": require('@/pages/shopDetail/[id].tsx').default
  },
  {
    "path": "/topicDetail/changes",
    "exact": true,
    "component": require('@/pages/topicDetail/changes.tsx').default
  },
  {
    "path": "/topicDetail/tomore",
    "exact": true,
    "component": require('@/pages/topicDetail/tomore.tsx').default
  },
  {
    "path": "/topicDetail/:id",
    "exact": true,
    "component": require('@/pages/topicDetail/[id].tsx').default
  },
  {
    "path": "/",
    "routes": [
      {
        "path": "/index/car",
        "exact": true,
        "component": require('@/pages/index/car.tsx').default,
        "wrappers": [require('@/wrappers/auth').default]
      },
      {
        "path": "/index/classify",
        "exact": true,
        "component": require('@/pages/index/classify.tsx').default
      },
      {
        "path": "/index/home",
        "exact": true,
        "component": require('@/pages/index/home.tsx').default
      },
      {
        "path": "/index/my",
        "exact": true,
        "component": require('@/pages/index/my.tsx').default,
        "wrappers": [require('@/wrappers/auth').default]
      },
      {
        "path": "/index/topic",
        "exact": true,
        "component": require('@/pages/index/topic.tsx').default
      }
    ],
    "component": require('@/pages/index/_layout.tsx').default
  }
];

  // allow user to extend routes
  plugin.applyPlugins({
    key: 'patchRoutes',
    type: ApplyPluginsType.event,
    args: { routes },
  });

  return routes;
}
