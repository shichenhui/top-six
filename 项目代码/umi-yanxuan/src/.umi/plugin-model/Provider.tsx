// @ts-nocheck
import React from 'react';
import initialState from 'C:/Users/宋超/Desktop/umi-market/src/.umi/plugin-initial-state/models/initialState';
import model0 from "C:/Users/宋超/Desktop/umi-market/src/models/brand";
import model1 from "C:/Users/宋超/Desktop/umi-market/src/models/home";
import model2 from "C:/Users/宋超/Desktop/umi-market/src/models/shopDet";
import model3 from "C:/Users/宋超/Desktop/umi-market/src/models/topic";
import model4 from "C:/Users/宋超/Desktop/umi-market/src/models/topicDetail";
import model5 from "C:/Users/宋超/Desktop/umi-market/src/models/typelist";
import model6 from "C:/Users/宋超/Desktop/umi-market/src/models/user";
// @ts-ignore
import Dispatcher from 'C:/Users/宋超/Desktop/umi-market/node_modules/@umijs/plugin-model/lib/helpers/dispatcher';
// @ts-ignore
import Executor from 'C:/Users/宋超/Desktop/umi-market/node_modules/@umijs/plugin-model/lib/helpers/executor';
// @ts-ignore
import { UmiContext } from 'C:/Users/宋超/Desktop/umi-market/node_modules/@umijs/plugin-model/lib/helpers/constant';

export const models = { '@@initialState': initialState, 'brand': model0, 'home': model1, 'shopDet': model2, 'topic': model3, 'topicDetail': model4, 'typelist': model5, 'user': model6 };

export type Model<T extends keyof typeof models> = {
  [key in keyof typeof models]: ReturnType<typeof models[T]>;
};

export type Models<T extends keyof typeof models> = Model<T>[T]

const dispatcher = new Dispatcher!();
const Exe = Executor!;

export default ({ children }: { children: React.ReactNode }) => {

  return (
    <UmiContext.Provider value={dispatcher}>
      {
        Object.entries(models).map(pair => (
          <Exe key={pair[0]} namespace={pair[0]} hook={pair[1] as any} onUpdate={(val: any) => {
            const [ns] = pair as [keyof typeof models, any];
            dispatcher.data[ns] = val;
            dispatcher.update(ns);
          }} />
        ))
      }
      {children}
    </UmiContext.Provider>
  )
}
