/*
 * @Author: your name
 * @Date: 2020-11-18 20:43:12
 * @LastEditTime: 2020-11-20 21:00:30
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \umi-yanxuan\src\utils\index.ts
 */
import Cookie from "js-cookie"

const key =  "x-nideshop-token";

//获取登录态
export function getToken(){
    return Cookie.get(key)
}

//设置登录态
export function setToken(val:string){
    Cookie.set(key,val)
}

//删除登录态
export function delToken(){
    Cookie.remove(key)
}
