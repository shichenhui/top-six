import React from 'react';
import { useEffect, useState } from 'react'
import { IRouteComponentProps, useModel } from 'umi'
import { IBrand } from '@/utils/types'
import { LeftOutlined } from '@ant-design/icons';
import styles from './brand.less'
// import Item from 'antd/lib/list/Item';
const Brand: React.FC<IRouteComponentProps> = (props: any) => {
    let { getBrnDet } = useModel('brand', model => ({ getBrnDet: model.getBrnDet }));
    let [branchData, setBranch] = useState({} as IBrand)
    useEffect(() => {
        getBranc()
    }, [getBrnDet])
    async function getBranc() {
        let res = await getBrnDet(props.match.params.id)
        setBranch(res)
        console.log(res);

    }
    //返回首页
    async function backHome() {
        props.history.go(-1)
    }
    return (
        <div className={styles.con}>
            <div className={styles.head}>
                <LeftOutlined onClick={backHome} />
                <span>{branchData.name}</span>
                <span>  </span>
            </div>
            <img src={branchData.app_list_pic_url} alt="" />
            <p>{branchData.simple_desc}</p>
        </div>
    );
}

export default Brand;