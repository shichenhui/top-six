import React from 'react'
import Styles from './css/url.less'
import { history } from 'umi'
import { Input, Cascader, Form, Switch, } from 'antd';
import {useState} from 'react'
const urladd: React.FC = () => {
    const options = [
        {
            value: 'zhejiang',
            label: 'Zhejiang',
            children: [
                {
                    value: 'hangzhou',
                    label: 'Hangzhou',
                    children: [
                        {
                            value: 'xihu',
                            label: 'West Lake',
                        },
                    ],
                },
            ],
        },
        {
            value: 'jiangsu',
            label: 'Jiangsu',
            children: [
                {
                    value: 'nanjing',
                    label: 'Nanjing',
                    children: [
                        {
                            value: 'zhonghuamen',
                            label: 'Zhong Hua Men',
                        },
                    ],
                },
            ],
        },
    ];
    function onChange(value: any) {
        console.log(value);
    }
  
    let [name,nameaddvalues]=useState([])
    let [pass,passvalues]=useState([])
    let [addressvalues,getaddvalues]=useState([])

    function getaddvaluess(e: any){
        console.log(e)
        getaddvalues(e)
    }
    console.log(addressvalues)
    return <div className={Styles.wrap}>
        <div className={Styles.left}>新增地址</div>
        <div className={Styles.center}>
            <Input placeholder="姓名" bordered value={addressvalues} onChange={(e)=>getaddvaluess(e.target.value)}/>
            <Input placeholder="手机号" bordered value={name}  onChange={(e)=>getaddvaluess(e.target.value)}/>
            <Cascader
                size='large'
                className={Styles.cas}
                bordered
                defaultValue={['zhejiang', 'hangzhou', 'xihu']}
                placeholder="11"
                options={options}
                onChange={onChange}
            />
            <Input placeholder="详细地址" bordered value={pass} onChange={(e)=>getaddvaluess(e.target.value)} />
            <Form.Item label="设置默认地址">
                <Switch />
            </Form.Item>
        </div>
        <div className={Styles.right}>
            <button onClick={() => history.go(-1)}>取消</button>
            <button>确定</button>
        </div>
    </div>
}

export default urladd