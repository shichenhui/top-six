import React from 'react'
import { useModel,history } from 'umi'
import Styles from '@/pages/css/detail.less'

const Home:React.FC=()=>{
    function goback(){
        history.go(-1)
    }

    return <div className={Styles.big}>
        <header>
            <span className={Styles.spone} onClick={()=>{goback()}} >＜</span>
            <span className={Styles.sptwo}>啊大大大</span>
        </header>
        <main>2</main>
        <footer>
            <div className={Styles.one}>
                <i className='icon iconfont icon-zhuanti'></i>
                <i className='icon iconfont icon-gouwuche'></i>
            </div>
            <div className={Styles.two}>
                <button className={Styles.btnone}>加入购物车</button>
                <button className={Styles.btntwo}>立即购买</button>
            </div>
        </footer>
    </div>
}

export default Home