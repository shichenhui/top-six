import React from "react"
import styles from "@/pages/home/subject.less"
import {useState,useEffect} from "react"
import {useModel} from "umi"
import {IRouteComponentProps,history} from "umi"

let Esti:React.FC<IRouteComponentProps> = ({match}:any)=>{
    let [eatiList,setEatiList] = useState([])

    //获取评价
    let {getSubjectEstis} = useModel("subject",model=>({getSubjectEstis:model.getSubjectEstis}))
    
    async function eatiLists(){
        let result = await getSubjectEstis({valueId:match.params.id,typeId:1,page:1,size:100});
        setEatiList(result.data.data)
    }

    useEffect(()=>{
        eatiLists()
    },[getSubjectEstis])

    return (
        <div className={styles.eatiContainer}>
            <div className={styles.eatiHeader}>
                <span onClick={()=>history.go(-1)}>&lt;</span>查看更多评论
            </div>
            <div className={styles.eatiMain}>
                {
                    eatiList.map((item:any)=>{
                        return <li key={item.id}>
                            <div>
                                <span>匿名用户</span>
                                <span>{item.add_time}</span></div>

                            <div><span>{item.content}</span></div>
                        </li>
                    })
            }
            </div>
           
        </div>
    )
}
export default Esti;