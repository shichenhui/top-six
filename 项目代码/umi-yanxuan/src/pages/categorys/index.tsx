import React from 'react'
import { history } from 'umi'
import styles from './index.less'
import { useState, useEffect } from 'react';
import { getClassifyList,getClassifyContentList } from '@/api/classify';

const Categorys:React.FC = () => {
  let id = history.location.query.id
  let index = history.location.query.index
  let itemid = history.location.query.item
  let [num, setIndex] = useState(index);
  let [list, setList] = useState([]);
  let [banner, setBanner] = useState([]);
  let [rightList, setRightList] = useState([]);
  useEffect(() => {
    const tab = async (index:number) => {
      const result = await getClassifyList(id);
      const list = await getClassifyContentList(itemid);
      setRightList(list.data.data)
      setList(result.data.currentCategory.subCategoryList)
      setBanner(result.data.currentCategory.subCategoryList[index])
    };
    tab(index)
  }, []);
  
  const tabCurrent = async (item: any) => {
    const list = await getClassifyContentList(item.id);
    setBanner(item)
    setRightList(list.data.data);
  };

  function goDetail(id:number){
    history.push(`/detail/${id}`)
  }

  return (
    <div className={styles.noTabPageContent}>
      <div className={styles.header}>
        <div className={styles.left} onClick={()=>{history.goBack(-1)}}>&lt;</div>
        <div className={styles.title}>奇趣分类</div>
        <div className={styles.right}></div>
      </div>
      <div className={styles.tabWrap}>
        {
          list && list.map((item:any,index:number) => {
            return <p key={item.id} className={num === index ? styles.active : ''}
              onClick={()=>{
                tabCurrent(item);
                setIndex(index);
              }}>
                {item.name}
            </p>
          })
        }
      </div>
      <div className={styles.content}>
        <div className={styles.categoryDetail}>
          <div>{banner.name}</div>
          <div>{banner.front_name}</div>
        </div>
        <div className={styles.goodsList}> 
          {
            rightList &&rightList.map((item:any,index:number) => {
              return <dl key={index} onClick={()=>goDetail(item.id)}>
                <dt>
                  <img src={item.list_pic_url} alt=""/>
                </dt>
                <dd>
                  <h4>{item.name}</h4>
                  <p>￥{item.retail_price}元</p>
                </dd>
              </dl>
            })
          }
        </div>
      </div>
    </div>
  )
}

export default Categorys