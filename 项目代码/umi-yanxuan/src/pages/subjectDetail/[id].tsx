import React from "react"
import styles from "@/pages/home/subject.less"
import {IRouteComponentProps} from "umi"
import {useState,useEffect} from "react"
import {useModel} from "umi"
import {history} from "umi"

const Subjectid:React.FC<IRouteComponentProps> = ({match}:any)=>{
    let [subjectHeader,setSubjectDetail] = useState([])//标题
    let [subjectImg,setsubjectImg] = useState([])//图片详情
    let [subjectEsti,setsubjectEsti] = useState([])//评价
    let [subjects,setsubjects] = useState([])//推荐专题

    
    let {getSubjectDetails,getSubjectEstis,getSubjectRecommends} = useModel("subject",model=>(
            {
                getSubjectDetails:model.getSubjectDetails,//获取图片详情
                getSubjectEstis:model.getSubjectEstis,//获取评价
                getSubjectRecommends:model.getSubjectRecommends//获取推荐
            }
        ))
    async function subjectDetails(){
        //获取图片详情
        let result = await getSubjectDetails({id:match.params.id}) 
        //获取评价
        let result1 = await getSubjectEstis({valueId:result.data.id,typeId:result.data.is_show})
        //推荐
        let result2 = await getSubjectRecommends({id:result.data.id})
        setSubjectDetail(result.data.title)//标题
        setsubjectImg(result.data.content)//详情图
        setsubjectEsti(result1.data.data)//评价
        setsubjects(result2.data)//推荐
    } 

    useEffect(()=>{
        subjectDetails()
    },[getSubjectDetails,getSubjectEstis,getSubjectRecommends])

    function createMarkup(str:string) {
        return {__html: str};
      }
      
    function MyComponent(str:string) {
        return <div dangerouslySetInnerHTML={createMarkup(str)} />;
    }


    return (
        <div className={styles.subujectDeatil}>
            <div className={styles.detailHeader}><span onClick={()=>history.go(-1)}>&lt;</span>{subjectHeader}</div>
            <div className={styles.detailMain}>
                <div>
                    {MyComponent (subjectImg as unknown as string)}
                </div>
                    {
                         subjectEsti.length?<ul>
                         {
                             subjectEsti.map((item:any)=>{
                                 return <li key={item.id}>
                                     <div>
                                         <span>匿名用户</span>
                                         <span>{item.add_time}</span></div>
     
                                     <div><span>{item.content}</span></div>
                                 </li>
                             })
                         }
                         <li onClick={()=>history.push(`/esti/${match.params.id}`)}>查看更多评论</li>
                     </ul>:""
                    }
               
                <div>
                    {
                        subjects.map((item:any)=>{
                            return <dl key={item.id}>
                                <dt><img src={item.scene_pic_url} alt=""/></dt>
                                    <dd>{item.title}</dd>
                            </dl>
                        })
                        
                    }
                </div>
            </div>
        </div>
    )
}
export default Subjectid