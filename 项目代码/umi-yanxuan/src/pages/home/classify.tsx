import React from 'react';
import styles from './classify.less';
import { useState, useEffect } from 'react';
import { getClassList, getClassifyList } from '@/api/classify';
import {history} from 'umi'

const Classify: React.FC = () => {
  let [num, setIndex] = useState(0);
  let [list, setList] = useState([]);
  let [banner, setBanner] = useState([]);
  let [rightList, setRightList] = useState([]);
  useEffect(() => {
    const tab = async () => {
      const result = await getClassList();
      setList(result.data.categoryList);
      setBanner(result.data.categoryList[0]);
      setRightList(result.data.categoryList[0].subCategoryList);
    };
    tab();
  }, []);
  const tabCurrent = async (item: any) => {
    const list = await getClassifyList(item.id);
    setBanner(list.data.currentCategory);
    setRightList(list.data.currentCategory.subCategoryList);
  };

  function goDetail(index:number,item:any){
    history.push({
      pathname:`/categorys`,
      query:{
        id:banner.id,
        index:index,
        item:item.id
      }
    })
  }

  return (
    <div className={styles.classify}>
      <div className={styles.searchWrap}>
        <div className={styles.searchInput}>
          <span>搜索商品，共239款好物</span>
        </div>
      </div>
      <div className={styles.con}>
        <div className={styles.left}>
          {list &&
            list.map((item: any, index: number) => {
              return (
                <p
                  key={item.id}
                  className={num === index ? styles.active : ''}
                  onClick={() => {
                    tabCurrent(item);
                    setIndex(index);
                  }}
                >
                  {item.name}
                </p>
              );
            })}
        </div>
        <div className={styles.right}>
          <div className={styles.right_banner}>
            <img src={banner.banner_url} alt="" />
            <span>{banner.front_desc}</span>
          </div>
          <h4>—— {banner.name}分类 ——</h4>
          <div className={styles.list}>
            {rightList &&
              rightList.map((item: any, index: any) => {
                return (
                  <dl key={index}>
                    <dt>
                      <img src={item.wap_banner_url} alt="" onClick={()=>goDetail(index,item)} />
                    </dt>
                    <dd>
                      <p>{item.name}</p>
                    </dd>
                  </dl>
                );
              })}
          </div>
        </div>
      </div>
    </div>
  );
};
export default Classify;



// import React,{ useState, useEffect } from 'react'
// import { useModel } from 'umi'
// import { Tabs } from 'antd';
// import { getClassifyList } from '@/api/classify'

// const Classify:React.FC = () => {
//   let { TabPane } = Tabs;
//   let [tabLeft,updateLeft] = useState([])
//   let [tabRight,updateRight] = useState([])
//   let { classList,getClassListData } = useModel('classify',model => ({classList:model.classList,getClassListData:model.getClassListData}))
//   useEffect(() => {
//     getClassList()
//   }, [getClassListData])

//   async function getClassList() {
//     let result = await getClassListData()
//     updateLeft(result.categoryList)
//     console.log(result)
//   }


 
//   function callback(index:number) {
//     console.log(index,'123');
//   }

//   return (
//     <div>
//       <Tabs defaultActiveKey="1" tabPosition={"left"} onTabClick={()=>callback}>
//         {
//           tabLeft.map((item:any,index:number)=>{
//             return <TabPane tab={item.name} key={index} >
//               {JSON.stringify(item.subCategoryList)}
//             </TabPane>
//           })
//         }
//       </Tabs>
//     </div>
//   )
// }

// export default Classify
