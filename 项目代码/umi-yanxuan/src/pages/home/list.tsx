import React from 'react'
import { useModel } from 'umi'
import { useState, useEffect } from 'react'
import { Carousel } from 'antd';
import '../css/list.less'
import '@/font/iconfont.css'
import {history} from 'umi'
const Home: React.FC = () => {
    let [banners, setData] = useState([])
    let [tolist, settolist] = useState([])
    let [list, getlist] = useState([])
    let [listhook, ListHooks] = useState([])
    let [goodlistall, goodlist] = useState([])
    let [data, goodlistaes] = useState([])


    let { Idatalist, DataList } = useModel('user');
    useEffect(() => {
        getDataList()
    }, [DataList]);

    async function getDataList() {
        const result = await DataList()
        //swiper
        setData(result.data.banner);
        //列表
        settolist(result.data.channel);
        //列表二
        getlist(result.data.brandList);
        //列表三
        ListHooks(result.data.newGoodsList);
        goodlist(result.data.hotGoodsList);
        goodlistaes(result.data.categoryList);
 
    }
    function dedail(id:number){

        console.log(id,'id')
        history.push({
            pathname:"/dedail",
            query:{
               id
            },
            hash:'id8',
            state:id,
        })
    }
    return <div>
        <div className="left">
            <div className="top">
                <Carousel autoplay>
                    {
                        banners.map((item: any) => {

                            return <img src={item.image_url} alt="" key={item} />
                        })
                    }
                </Carousel>
            </div>
            <div className="bottom">
                <ul>
                    {
                        tolist.map((item: any) => {

                            return <li key={item.id} className=''>
                                <img src={item.icon_url} alt="" />
                                <p>
                                    <a href="#">{item.name}</a>
                                </p>
                            </li>
                        })
                    }
                </ul>
            </div>
        </div>
        <h1 className='h1'>品牌制造商直供</h1>
        <div className="center">
            {
                list.map((item: any) => {

                    return <ul key={item.id}>
                        <li>
                            <img src={item.pic_url} alt="" />
                            <a href="#">{item.name}</a>
                            <p>{item.floor_price}元起</p>
                        </li>
                    </ul>
                })
            }
        </div>
        <h1 className='h1'>新品首发</h1>
        <div className="container">
            {
                listhook.map((item: any) => {

                    return <dl key={item.id}>
                        <dt>
                            <img src={item.list_pic_url} alt="" />
                        </dt>
                        <dd>
                            <p>{item.name}</p>
                        </dd>
                    </dl>
                })
            }
        </div>
        <h1 className='h1'>人气推荐</h1>

        <div className="wrap">
            {
                goodlistall.map((item: any) => {

                    return <dl key={item.id}>
                        <dt>
                            <img src={item.list_pic_url} alt="" onClick={()=>dedail(item.id)}/>
                        </dt>
                        <dd>
                            <b>{item.name}</b>
                            <p>{item.goods_brief}</p>
                            <p>￥{item.retail_price}</p>
                        </dd>

                    </dl>
                })
            }
        </div>
        <div className="home">
          
        </div>
    </div>
}


export default Home