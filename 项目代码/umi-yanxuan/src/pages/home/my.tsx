import React from 'react'
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { Toast, WhiteSpace, WingBlank, Button } from 'antd-mobile';
import Styles from '../css/my.less'
import '@/font/iconfont.css'
import { Link ,history} from 'umi';
import {IDataList} from '@/util/types'
const Home: React.FC = () => {
    function remlogin() {
        history.push('/login');
    }
    function order(){
        Toast.offline('我的订单还未解锁,请耐心等候', 2);
    }
    function order1(){
        Toast.offline('周末拼单还未解锁,请耐心等候', 2);
    }
    function order2(){
        Toast.offline('优惠券还未解锁,请耐心等候', 2);
    }
    function order3(){
        Toast.offline('优选购还未解锁,请耐心等候', 2);
    }
    function order4(){
        Toast.offline('我的红包还未解锁,请耐心等候', 2);
    }
    function order5(){
        Toast.offline('会员plus还未解锁,请耐心等候', 2);
    }
    function order6(){
        Toast.offline('邀请返利还未解锁,请耐心等候', 2);
    }
    function order7(){
        Toast.offline('意见反馈还未解锁,请耐心等候', 2);
    }
    function order8(){
        Toast.offline('客服咨询还未解锁,请耐心等候', 2);
    }
    function order9(){
        Toast.offline('账号安全还未解锁,请耐心等候', 2);
    }
    function goAddress(){
        history.push('/detail/detail')
    }
    
        return <div className={Styles.box}>
        <div className={Styles.left}>

            <dl>
                <dt>
                    <Avatar size={84} icon={<UserOutlined />} style={{ color: '#ddd', backgroundColor: '#ccc' }} />
                </dt>
                <dd>
                    <p>16601538454</p>
                    <p>普通用户</p>
                </dd>
            </dl>
        </div>
        <div className={Styles.conter}>
            <ul>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>
                    
                    <Link to='#'>我的收藏</Link>
                </li>
                <li onClick={()=>{goAddress()}}>
     
                    <i className='icon iconfont icon-zhuanti'></i>
                    <Link to='#' >我的地址管理</Link>
                </li>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>

                    <p onClick={()=>{order()}}>我的订单</p>
                </li>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>
                    <p onClick={()=>{order1()}}>周末拼单</p>
                </li>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>
                    <p onClick={()=>{order2()}}>优惠券</p>
                </li>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>
                    <p onClick={()=>{order3()}}>优选购</p>
                </li>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>
                    <p onClick={()=>{order4()}}>我的红包</p>
                </li>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>
                    <p onClick={()=>{order5()}}>会员plus</p>
                </li>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>
                    <p onClick={()=>{order6()}}>邀请返利</p>
                </li>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>
                    <p onClick={()=>{order7()}}>意见反馈</p>
                </li>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>
                    <p onClick={()=>{order8()}}>客服咨询</p>
                </li>
                <li>
                    <i className='icon iconfont icon-zhuanti'></i>
                    <p onClick={()=>{order9()}}>账号安全</p>
                </li>
            </ul>
        </div>
        <div className={Styles.btn}>
            <button onClick={() => remlogin()}>退出登录</button>
        </div>
    </div>
}

export default Home