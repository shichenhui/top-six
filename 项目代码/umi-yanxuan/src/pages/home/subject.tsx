import React from 'react';
import styles from '@/pages/home/subject.less';
import {useState,useEffect} from "react"
import {useModel} from "umi"
import {history} from "umi"

export default () => {
  let [subjectList,setSubjectList] = useState([])

  //获取专题数据
  let {getSubjectLists} = useModel("subject",model=>({getSubjectLists:model.getSubjectLists}))

  //存储数据
  async function subjectLists() {
    let result = await getSubjectLists()
    setSubjectList(result.data.data)
    console.log(result)
  }

  //跳转详情，动态传参id
  function clickSubjectDetail(id:number){
    history.push(`/subjectDetail/${id}`)
  }

  useEffect(()=>{
    subjectLists()
  },[getSubjectLists])

  return (
    <div className={styles.subjectContainer}>
        {
          subjectList.map((item:any)=>{
            return <dl key={item.id} onClick={()=>clickSubjectDetail(item.id)}>
              <dt><img src={item.scene_pic_url} alt=""/></dt>
              <dd>
                <span>{item.title}</span>
                <span>{item.subtitle}</span>
                <span>{item.price_info}元起</span>
              </dd>
            </dl>
          })
        }
    </div>
  );
}
