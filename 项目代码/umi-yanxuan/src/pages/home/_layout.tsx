import React from 'react'
import { IRouteComponentProps, Link } from 'umi'
import Styles from '@/pages/css/index.less'
import '@/font/iconfont.css'
const Layout: React.FC<IRouteComponentProps> = function (props) {
  
    return (
        <>
            <main>
                {props.children}
            </main>
            <footer className={Styles.footer}>
                <ul >
                    <li>
                        <i className='icon iconfont icon-activeHome-copy-copy'></i>
                        <Link to='/home/list'>首页</Link>
                    </li>
                    <li>
                        <i className='icon iconfont icon-zhuanti'></i>
                        <Link to='/home/subject'>专题</Link>
                    </li>
                    <li>
                        <i className='icon iconfont icon-leimupinleifenleileibie'></i>
                        <Link to='/home/classify'>分类</Link>
                    </li>
                    <li>
                        <i className='icon iconfont icon-gouwuche'></i>
                        <Link to='/home/cart'>购物车</Link>
                    </li>
                    <li>
                        <i className='icon iconfont icon-wode'></i>
                        <Link to='/home/my'>我的</Link>
                    </li>
                </ul>

            </footer>
        </>
    );
}

export default Layout