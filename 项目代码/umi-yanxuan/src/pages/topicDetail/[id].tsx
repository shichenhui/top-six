import React from 'react'
import styles from "../index/topic.less"
import { ArrowLeftOutlined,FormOutlined} from '@ant-design/icons';
import  {useState,useEffect} from "react"
import {useModel,IRouteComponentProps} from "umi"

// import { IRouteComponentProps } from 'umi'


// const Cart: React.FC<IRouteComponentProps> = ({match}:any) => {
//   return (
//     <div>
//       <h1>详情页面：{JSON.stringify(match)}</h1>
//       {match.params.id}
//     </div>
//   );
// }
// export default Cart;
const topDetail:React.FC<IRouteComponentProps> = (props:any) =>  {

    
    const { getTopicDetail,getTopicRelated} = useModel('topicDetail', model => ({ getTopicDetail: model.getTopicDetail,getTopicRelated: model.getTopicRelated}));
    let [topicDetaillist,setTopicDetaillist]=useState('')
    let [topicTitle,setTopicTitle]=useState('')
    let [topicTopicRelated,setTopicRelated]=useState([])
    let [id,useId]=useState(props.match.params.id)
    useEffect(() => {
        topicDetail(),
        topicRelated()
    }, [])

    async function topicDetail(){
        let res=await getTopicDetail(id)
        setTopicDetaillist(res.data.content)
        setTopicTitle(res.data.title)
        // console.log(res.data.content,'aaaaaaaaaaaaaa')
    }

    async function topicRelated(){
        let res=await getTopicRelated()
        setTopicRelated(res.data)
        console.log(res.data)
    }
   async function backs(){
       console.log(props);
       
       props.history.go(-1)
   }
   async function tochanges(){
       props.history.push('/topicDetail/changes')
   }
   async function tomore(){
    props.history.push('/topicDetail/tomore')
   }
    return(
        <div className={styles.lists}>
            <div className={styles.top}>
                <ArrowLeftOutlined className={styles.left} onClick={()=>{backs()}}/>
                <span>{topicTitle}</span>
                <span></span>
            </div>

          <div className={styles.imgs}>
                <div dangerouslySetInnerHTML = {{__html:topicDetaillist}} ></div>
          </div>

        <div className={styles.built}>
                <ul onClick={()=>{tochanges()}} className={styles.li1}>
                    <span>精选留言</span>
                    <FormOutlined className={styles.changes}/>
                </ul>
                <ul>
                    <li>
                        <span>匿名用户</span>
                        <span>2017-05-15  10.06.01</span>
                    </li>
                    <li>
                        是记忆棉 很满意
                    </li>
                </ul>
                <ul>
                    <li>
                        <span>匿名用户</span>
                        <span>2017-05-15  10.06.01</span>
                    </li>
                    <li>
                        很好的东西
                    </li>
                </ul>
                <ul>
                    <li>
                        <span>匿名用户</span>
                        <span>2017-05-15  10.06.01</span>
                    </li>
                    <li>
                        很舒服，有没有那么的柔软，不错
                    </li>
                </ul>
                <ul>
                    <li>
                        <span>匿名用户</span>
                        <span>2017-05-15  10.06.01</span>
                    </li>
                    <li>
                        确实舒服，不过夏天会不会热啊
                    </li>
                </ul>
                
        </div>

        <div className={styles.btn}>
            <button onClick={()=>{tomore()}}>查看更多评论</button>
        </div>

            {
                topicTopicRelated.map((item:any,index:number)=>{
                    return <div key={index} className={styles.detail2}>
                                <img src={item.scene_pic_url}/>
                                <span>{item.title}</span>
                                <p>{item.subtitle}</p>
                                <li>{item.price_info}元/起</li>
                    </div>
                  
                })
            }
        </div>
    )
}

export default topDetail

