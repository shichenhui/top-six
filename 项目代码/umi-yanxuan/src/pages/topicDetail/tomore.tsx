import React from 'react'
import styles from "../index/topic.less"
import  {useState,useEffect} from "react"
import {useModel,IRouteComponentProps} from "umi"
import { ArrowLeftOutlined} from '@ant-design/icons';

const topDetail:React.FC<IRouteComponentProps> = (props:any) =>  {
  
   async function backs(){
       props.history.go(-1)
   }
    return(
        <div className={styles.lists}>
            <div className={styles.top}>
                <ArrowLeftOutlined className={styles.left} onClick={()=>{backs()}}/>
                <span>查看更多评论</span>
                <span></span>
            </div>
        </div>
    )
}

export default topDetail

