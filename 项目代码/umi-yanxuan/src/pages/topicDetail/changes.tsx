import React from 'react'
import styles from "../index/topic.less"
import  {useState,useEffect} from "react"
import {useModel,IRouteComponentProps} from "umi"
import { ArrowLeftOutlined} from '@ant-design/icons';

const topDetail:React.FC<IRouteComponentProps> = (props:any) =>  {
   async function backs(){
       props.history.go(-1)
   }
    return(
        <div className={styles.lists}>
            <div className={styles.top}>
                <ArrowLeftOutlined className={styles.left} onClick={()=>{backs()}}/>
                <span>填写留言</span>
                <span></span>
            </div>

            <div className={styles.cons}>
                发布留言页面
            </div>
            <button>留言</button>
        </div>
    )
}

export default topDetail

