import React from 'react';
import { useEffect, useState } from 'react'
import { IRouteComponentProps, useModel } from 'umi'
import styles from './shopDet.less'
import { Carousel, Divider, Drawer, Button, Radio, Space } from 'antd';
import { StarOutlined } from '@ant-design/icons';
import Detail from '@/components/detailList'

const ShopDet: React.FC<IRouteComponentProps> = (props: any) => {
    let { getRelate, getShopDetail, getCarCount } = useModel('shopDet', model => ({ getRelate: model.getRelate, getShopDetail: model.getShopDetail, getCarCount: model.getCarCount }));
    let [relate, setRelate] = useState([]) //商品推荐列表
    let [shopDetail, setShopDetail] = useState({} as any) //商品详情内容
    let [carCount, setCarCount] = useState(0) //购物车商品数量
    let [gallery, setGallery] = useState([]) //轮播图数据
    let [attribute, setAttribute] = useState([]) //商品参数数据
    let [issue, setIssue] = useState([]) //常见问题
    let [visible, setVisible] = useState(false) //弹框显示
    let [placement, setPlacement] = useState("bottom") //弹框方向
    useEffect(() => {
        getRelateData()
        getShopDetailData()
        getCarCountData()
    }, [getRelate, getRelate, getCarCount])
    async function getRelateData() {
        let res = await getRelate(props.match.params.id)
        setRelate(res)
    }
    async function getShopDetailData() {
        let res = await getShopDetail(props.match.params.id)
        setShopDetail(res)
        setGallery(res.gallery)
        setAttribute(res.attribute)
        setIssue(res.issue)
        console.log();

    }
    async function getCarCountData() {
        let res = await getCarCount()
        setCarCount(res)
    }
    function showDrawer() {
        setVisible(true)
    };
    function onClose() {
        setVisible(false)
    };
    return (
        <div className={styles.con}>
            <header>
                {shopDetail.info && shopDetail.info.name}
            </header>
            <main>
                <div className="swiper">
                    <Carousel autoplay>
                        {gallery.map((item: any) => {
                            return <img src={item.img_url} key={item.id} />
                        })}
                    </Carousel>
                </div>
                <ul className={styles.serve}>
                    <li><span>★</span>30天无忧退货</li>
                    <li><span>★</span>48小时快速退款</li>
                    <li><span>★</span>满88元免邮费</li>
                </ul>
                <div className={styles.msg}>
                    <Divider className={styles.thro}>商品参数</Divider>
                    {attribute.map((item: any, index: number) => {
                        return <div key={index} className={styles.koa}>
                            <p>{item.name}</p>
                            <p>{item.value}</p>
                        </div>
                    })}
                </div>
                <div dangerouslySetInnerHTML={{ __html: shopDetail.info && shopDetail.info.goods_desc }} ></div>
                <div className={styles.problem}>
                    <Divider className={styles.thro}>常见问题</Divider>
                    {
                        issue.map((item: any) => {
                            return <div key={item.id}>
                                <p><span>√</span>{item.question}</p>
                                <p>{item.answer}</p>
                            </div>
                        })
                    }
                </div>
                <div>
                    <Divider className={styles.thro}>大家都在看</Divider>
                    <div className={styles.relate}>
                        {relate.map((ite: any) => {
                            return <Detail ite={ite} key={ite.id} />
                        })}
                    </div>
                </div>
            </main>
           
            <Drawer
                title="Basic Drawer"
                placement="bottom"
                closable={false}
                onClose={() => { onClose() }}
                visible={visible}
                key={placement}
            >
                <p>Some contents...</p>
                <p>Some contents...</p>
                <p>Some contents...</p>
            </Drawer>
            <footer>
                <p><StarOutlined /></p>
                <p><i className='iconfont icon-che'></i></p>
                <p onClick={() => { showDrawer() }}>加入购物车</p>
                <p>立即购买</p>
            </footer>

        </div>
    );
}

export default ShopDet;