import React from 'react'
import { useModel,history } from 'umi'
import Styles from '@/pages/css/qiquclassify.less'

const Home:React.FC=()=>{

    function goback(){
        history.go(-1)
    }

    return <div className={Styles.box}>
        <header>
            <span className={Styles.spone} onClick={()=>{goback()}}>＜</span>
            <span className={Styles.sptwo}>奇趣分类</span>
        </header>
        <nav>
        
        </nav>
        <main>
        
        </main> 
    </div>
}

export default Home