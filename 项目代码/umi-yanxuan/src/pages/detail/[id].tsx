<<<<<<< HEAD
import React from 'react'
import { useEffect, useState } from "react"
import { IRouteComponentProps, useModel, history } from 'umi'
import Styles from '@/pages/css/detail.less'
import { Carousel } from 'antd'

const Home: React.FC<IRouteComponentProps> = (props: any) => {
	let { getRelate, getShopDetail } = useModel('detail', model => ({
		getRelate: model.getRelate, getShopDetail: model.getShopDetail
	}));
	let [relate, setRelate] = useState([]);
	let [shopDetail, setShopDetail] = useState<any>([]);
	useEffect(() => {
		getRelateDate()
		getShopDetailData()
	}, [getRelate, getShopDetail])
	async function getRelateDate() {
		let res = await getRelate(props.match.params.id)
		setRelate(res)
	}
	async function getShopDetailData() {
		let res = await getShopDetail(props.match.params.id)
		setShopDetail(res)
	}

	function goback() {
		history.go(-1)
	}

	return <div className={Styles.big}>
		<header>
			<div className={Styles.left} onClick={() => { history.goBack(-1) }}>&lt;</div>
			<div className={Styles.title}>{shopDetail.info && shopDetail.info.name}</div>
			<div className={Styles.right}></div>
		</header>
		<main>
			<div className={Styles.banner}>
      	<Carousel autoplay>
        	{
            shopDetail.gallery &&	shopDetail.gallery.map((item: any) => {
            	return <img src={item.img_url} alt="" key={item.id} />
            })
          }
        </Carousel>
      </div>
			<div className={Styles.divone}>
				<div>
					<h1>{shopDetail.info && shopDetail.info.name}</h1>
					<p>{shopDetail.info && shopDetail.info.goods_brief}</p>
					<p className={Styles.p}>￥{shopDetail.info && shopDetail.info.retail_price}</p>
				</div>
				<p className={Styles.size}>选择规格＞</p>
			</div>
			<div className={Styles.divtwo}>——商品参数——</div>
			<div className={Styles.divthree}>
				<p>——常见问题——</p>
				{
					shopDetail.issue && shopDetail.issue.map((item: any) => {
						return <div key={item.id}>
							<h1>{item.question}</h1>
							<div>{item.answer}</div>
						</div>
					})
				}
			</div>
			<div className={Styles.divfore}>
				{
					relate.map((item: any) => {
						return <dl key={item.id}>
							<dt><img src={item.list_pic_url} alt="" /></dt>
							<dd>
								<p>{item.name}</p>
								<p>￥{item.retail_price}</p>
							</dd>
						</dl>
					})
				}
			</div>
		</main>
		<footer>
			<div className={Styles.one}>
				<i className='icon iconfont icon-zhuanti'></i>
				<i className='icon iconfont icon-gouwuche'></i>
			</div>
			<div className={Styles.two}>
				<button className={Styles.btnone}>加入购物车</button>
				<button className={Styles.btntwo}>立即购买</button>
			</div>
		</footer>
	</div>
}

export default Home
=======
import { IRouteComponentProps, useModel } from 'umi'
import React from 'react';
import { useState, useEffect } from 'react'
import styles from './[id].less'

// import { LeftOutlined } from 'antd';
import { LeftOutlined } from '@ant-design/icons';
const Cart: React.FC=(props:any) => {
  let { getHome } = useModel('home', model => ({ getHome: model.getHome }));
  let [categoryList, setCategoryList] = useState([]) //切换数据
  let [typeList, updataList] = useState([])
  let [navIndex, carIndex] = useState(0)
  useEffect(() => {
    getHomeList()
  }, [getHome])
  async function getHomeList() {
    let res = await getHome()
    setCategoryList(res.data.categoryList)
    // updataId(res.data.categoryList[0].id)
    let id=props.match.params.id
    let index=res.data.categoryList.map((item:any)=>item.id).indexOf(+id)
    updataList(res.data.categoryList[index].goodsList)
    carIndex(index)
    console.log(index);
  }
  //点击头部tab切换
  async function tabContent(index: any) {
    carIndex(index)
    let res = await getHome();
    updataList(res.data.categoryList[index].goodsList)
    console.log(res.data.categoryList[index].goodsList);
  }
  //返回首页
  async function backHome() {
      props.history.go(-1)
  }
  return (
    <div className={styles.detail}>
      <div className={styles.posi}>
        <div className={styles.head}>
          <LeftOutlined onClick={backHome}/>
          <span>奇趣分类</span>
          <span>  </span>
        </div>
        <div className={styles.tab}>
          {
            categoryList.map((item: any, index: number) => {
              return <span key={item.id} onClick={() => { tabContent(index) }} className={index == navIndex ? `${styles.active}` : ''}> {item.name} </span>
            })
          }
        </div>
      </div>
      <div className={styles.content}>
        {
          typeList.map((item: any) => {
            return <li key={item.id}>
              <img src={item.list_pic_url} alt="" />
              <p>{item.name}</p>
              <p className={styles.price}> ￥{item.retail_price}元</p>
            </li>
          })
        }
      </div>
    </div>
  );
}


export default Cart;
>>>>>>> songchao
