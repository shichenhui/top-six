import React, { useState, useEffect } from 'react'
import { useModel, history } from 'umi'
import Styles from '@/pages/css/cartDetail.less'

const Home: React.FC = (props: any) => {
    let [detailList, setDetail] = useState();
    let [info, setInfor] = useState({ goods_brief: '',list_pic_url:'' });
    let [issue, setIssue] = useState([]);
    let [productList, setProductList] = useState([]);
    let { getDetail } = useModel('cart', model => ({ getDetail: model.getDetailData }))
    function goback() {
        history.go(-1)
    }
    async function getDetailData() {
        console.log(props.location.query.id)
        let id = props.location.query.id
        let res = await getDetail({ id: 1070000 })
        setDetail(res)
        setInfor(res.info)
        setIssue(res.issue)
        setProductList(res.productList)

    }
    console.log('productList', productList)
    useEffect(() => {
        getDetailData()
    }, [getDetail])

    return <div className={Styles.detail}>
        <header>
            <span className={Styles.spone} onClick={() => { goback() }} >＜</span>
            <span className={Styles.sptwo}>
                {info.goods_brief}</span>
        </header>
        <main>
       <div className={Styles.question}>
           <img src={info.list_pic_url} alt=""/>
       <h2>常见问题</h2>

       {
                issue.map((item: any) => {
                    return (
                        <div key={item.id}>
                            <h3>{item.question}</h3>
                            <p>{item.answer}</p>
                        </div>
                    )
                })
            }
       </div>
        <div className={Styles.look}>
        <h2>大家都在看</h2>
            {
                productList.map((item: any) => {
                    return (<div key={item.id}>{item.goods_number}</div>)
                })
            }
        </div>
        </main>
        <footer>
            <div className={Styles.one}>
                <i className='icon iconfont icon-zhuanti'></i>
                <i className='icon iconfont icon-gouwuche'></i>
            </div>
            <div className={Styles.two}>
                <button className={Styles.btnone}>加入购物车</button>
                <button className={Styles.btntwo}>立即购买</button>
            </div>
        </footer>
    </div>
}

export default Home