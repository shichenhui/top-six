import React from 'react'
import './css/login.less'
import { useModel } from 'umi'
import { useState } from 'react'
import { history } from 'umi';
import {setToken} from '@/util/index'
const Login: React.FC = () => {
    let [mobile, setMobie] = useState('15323807318')
    let [password, setPassword] = useState('123456')
    let { islogin, doLogin } = useModel('user', model => ({ islogin: model.islogin, doLogin: model.doLogin }));
    async function LoginClick() {
        if (!/^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013-8])|(18[0,5-9]))\d{8}$/.test(mobile)) {

            alert('请输入正确的手机号')
            return;
        }
        if (!password) {
            alert('请输入正确的密码')
            return;
        }
        let result = await doLogin({ mobile, password });
         console.log()
        history.push('/home/my');
    }


    return <div className='box'>
        <div className="name">
            <div className="left">
                <span>账号:</span>
                <input type="text" placeholder='请输入手机号码' value={mobile} onChange={(e) => setMobie(e.target.value)} />
                <a href="#">注册</a>
            </div>
            <br />
            <div className="center">
                <span>密码:</span>
                <input type="password" placeholder='请输入密码' value={password} onChange={(e) => setPassword(e.target.value)} />
                <a href="">忘记密码</a>
            </div>
            <br />
            <div className="right">
                <input type="checkbox" name="" id="" /><span>下次自动登陆</span>
            </div>
            <br />
            <button type="submit" onClick={LoginClick}>登陆</button>
        </div>
    </div>
}
export default Login