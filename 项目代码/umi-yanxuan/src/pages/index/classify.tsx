import React,{useEffect,useState} from 'react';
import {useModel} from 'umi';
import styles from './classify.less';
export default () => {
  let {getType,getTypeSingle} = useModel('typelist', model=>({getType: model.getType,getTypeSingle:model.getTypeSingle}));
  let [data, setData] = useState([]);
  let [id,updataId] = useState('');
  let [typeList,updataList] = useState([])
  useEffect( () => {
    typeClick()
}, []);
  async function typeClick(){
    
     let result = await getType();
    // console.log(result.data.categoryList[0].id)
     setData(result.data.categoryList);
     updataId(result.data.categoryList[0].id)
     let res = await getTypeSingle(result.data.categoryList[0].id)
     console.log(res.data.currentCategory.subCategoryList)
     //console.log(data,id)
     updataList(res.data.currentCategory.subCategoryList)
  }
  const listClick = async (id:string) =>{
        console.log(id)
        updataId(id)
        let res = await getTypeSingle(id);
        updataList(res.data.currentCategory.subCategoryList)
  }
  return (
    <div className={styles.type} >
     <div className={styles.left}>
       {(data as unknown as any).map((item:any)=>{
        return <li key={item.id} onClick={()=>listClick(item.id)}>{item.name}</li>
       })}
     </div> 
     <div className={styles.right}>
      {(typeList as unknown as any).map((item:any)=>{
        return <li key={item.id}>
                 <img src={item.wap_banner_url} alt=""/>
                 <p>{item.name}</p> 
               </li>
      })}
     </div> 
    </div>
  );
}
