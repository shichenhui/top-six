/*
 * @Description: 
 * @Version: 2.0
 * @Autor: 张磊
 * @Date: 2020-11-18 13:52:02
 * @LastEditors: 张磊
 * @LastEditTime: 2020-11-18 18:37:56
 * @FilePath: \marker\src\pages\index\_layout.tsx
 */
import React from 'react';
import { IRouteComponentProps, NavLink } from 'umi'
import styles from '../index.less';
import '@/public/fonts/iconfont.css'
interface INav {
    icon?: string
    path: string,
    text: string
}
const IndexLayout: React.FC<IRouteComponentProps> = function(props){
    const navList: Array<INav> = [
        {
            icon: 'iconfont icon-index',
            path: '/index/home',
            text: '首页'
        },
        {
            icon: 'iconfont icon-yinying',
            path: '/index/topic',
            text: '专题'
        },
        {
            icon: 'iconfont icon-leimupinleifenleileibie',
            path: '/index/classify',
            text: '分类'
        },
        {
            icon: 'iconfont icon-che',
            path: '/index/car',
            text: '购物车'
        },
        {
            icon: 'iconfont icon-wode',
            path: '/index/my',
            text: '我的'
        },
    ]
    return (
        <div className={styles.con}>
            <main>
                 {props.children}
            </main>
           
            <footer>
                <ul>
                    {navList.map((item, index) => {
                        return <li key={index}>
                            <NavLink to={item.path} activeClassName={styles.active}><i className={item.icon}></i>{item.text}</NavLink>
                        </li>
                    })}
                </ul>
            </footer>
        </div>
    );
}

export default IndexLayout;