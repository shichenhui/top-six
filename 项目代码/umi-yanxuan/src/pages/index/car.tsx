import { models } from '@/.umi/plugin-model/Provider';
import React,{useEffect,useState} from 'react';
import {useModel} from 'umi'
import styles from './car.less'
function Car() {
    let {getShop,shopsPort,delectShopData} = useModel('typelist',model=>({
        getShop:model.getShop,
        shopsPort:model.shopsPort,
        delectShopData:model.delectShopData
    }))
   let [shopList,updataShop] = useState([])
    useEffect(()=>{
        initData()
    },[])
   const initData = async () =>{
      let res = await getShop()
      console.log(res.data,2819);
      updataShop(res.data.cartList)
   }
   const del = async (pid:any) =>{
       console.log(pid,'delis')
      let res = await delectShopData(pid);
      console.log(res,'djdwiwdna')
   }
   const changCheck = async (isChecked:number,productId:string) =>{
      //  console.log(isChecked,productId)
    //     let obj = {
    //         isChecked,
    //         productId
    //     }
    //    let res =  await shopsPort(obj)
    let ar:any = []
    shopList.forEach((v:any)=>{
        if(v.id === productId){
            console.log(22) 
          v.checked  = !v.checked
        }
        // console.log(v.product_id)
        ar.push(v)
    })
    
   
    updataShop(ar)
   }
    return (
        <div>
            购物车
            <ul className={styles.title}>
                {
                    shopList.map((v:any)=>{
                        return <li> <div> <input type="checkbox" onClick={()=>changCheck(v.checked,v.id)}  checked ={v.checked} /> </div>  <img src={v.list_pic_url} alt=""  /> <div className={styles.div} >{v.goods_name} <p>￥{v.id}</p> <span onClick={()=>del(v.goods_id)}>删除</span>  </div>  </li>
                    })
                }
            </ul>
            
        </div>
    );
}
Car.wrappers = ['@/wrappers/auth']
export default Car;