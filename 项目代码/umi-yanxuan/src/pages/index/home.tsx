// import { getHomeList } from '@/api/home';
import React from 'react';
import { useState, useEffect } from 'react'
import { useModel } from 'umi'
import styles from './home.less'
import { Carousel } from 'antd';
import {
    RightCircleOutlined
} from '@ant-design/icons'
import Detail from '@/components/detailList'
// import Swiper from 'swiper'
// import 'swiper/css/swiper.css'
const Home: React.FC = (props: any) => {
    let { getHome } = useModel('home', model => ({ getHome: model.getHome }));
    let [banList, setBanList] = useState([]) //轮播图数据
    let [brandList, setBrandList] = useState([]) //品牌制造商
    let [newGoodsList, setNewGoodsList] = useState([]) //新品首发
    let [hotGoodsList, setHotwGoodsList] = useState([]) //人气推荐
    let [topicList, setTopicList] = useState([]) //专题精选
    let [categoryList, setCategoryList] = useState([]) //切换数据
    let [channel, setChannel] = useState([]) //nav数据
    useEffect(() => {
        getHomeList()
    }, [getHome])
    async function getHomeList() {
        let res = await getHome()
        setBanList(res.data.banner)
        setBrandList(res.data.brandList)
        setNewGoodsList(res.data.newGoodsList)
        setHotwGoodsList(res.data.hotGoodsList)
        setTopicList(res.data.topicList)
        setCategoryList(res.data.categoryList)
        setChannel(res.data.channel)
        console.log(res.data.categoryList);
    }
    function goDetail(id: number) {
        console.log(props, id);
        props.history.push(`/detail/${id}`)
    }
    //品牌制造业详情
    function goBranch(id: number) {
        props.history.push(`/brandDetail/${id}`)
    }
    //商品购物车详情
    function goShopDet(id: number) {
        props.history.push(`/shopDetail/${id}`)
    }
    return (
        <div className={styles.con}>
            {/* {JSON.stringify(banList) } */}
            <div className="swiper">
                <Carousel autoplay>
                    {banList.map((item: any) => {
                        return <img src={item.image_url} key={item.id} />
                    })}
                </Carousel>
            </div>
            <div className={styles.nav}>
                <ul>
                    {channel.map((item: any) => {
                        return <li key={item.id} onClick={() => { goDetail(item.id) }}>
                            <img src={item.icon_url} alt="" />
                            <span>{item.name}</span>
                        </li>
                    })}
                </ul>
            </div>
            <div className='list_box'>
                <p className="title">
                    品牌制造商直供
                    </p>
                <div>
                    {brandList.map((item: any) => {
                        return <div key={item.id} className={styles.bran_list_item} onClick={() => { goBranch(item.id) }}>
                            <img src={item.new_pic_url} alt="" />
                            <div>
                                <p>{item.name}</p>
                                <p>￥{item.floor_price}</p>
                            </div>
                        </div>
                    })}
                </div>
            </div>
            <div className='list_box'>
                <p className="title">
                    新品首发
                    </p>
                <div>
                    {newGoodsList.map((item: any) => {
                        return <div key={item.id} className={styles.new_list_item} onClick={() => { goShopDet(item.id) }} >
                            <img src={item.list_pic_url} alt="" />
                            <div>
                                <p>{item.name}</p>
                                <p>￥{item.retail_price}</p>
                            </div>
                        </div>
                    })}
                </div>
            </div>
            <div className='list_box'>
                <p className="title">
                    人气推荐
                    </p>
                <div>
                    {hotGoodsList.map((item: any) => {
                        return <div key={item.id} className={styles.hot_list_item}>
                            <img src={item.list_pic_url} alt="" />
                            <div>
                                <p>{item.name}</p>
                                <p>{item.goods_brief}</p>
                                <p>￥{item.retail_price}</p>
                            </div>
                        </div>
                    })}
                </div>
            </div>
            <div className='list_box'>
                <p className="title">
                    专题精选
                    </p>
                <div className={styles.topSwip}>
                    <Carousel>
                        {topicList.map((item: any) => {
                            return <img src={item.item_pic_url} key={item.id} />
                        })}
                    </Carousel>
                </div>

            </div>
            <div className={styles.cat}>
                {categoryList.map((item: any) => {
                    return <div key={item.id}>
                        <p className={styles.car_title}>{item.name}</p>
                        <div className={styles.cat_list}>
                            {item.goodsList.map((ite: any) => {
                                return <Detail ite={ite} key={ite.id}/>
                            })}
                            <dl onClick={() => { goDetail(item.id) }}>
                                <dt><RightCircleOutlined className={styles.cat_icon} /></dt>
                                <dd><p>更多{item.name}好物</p></dd>
                            </dl>
                        </div>
                    </div>
                })}
            </div>
        </div>
    );
}
export default Home