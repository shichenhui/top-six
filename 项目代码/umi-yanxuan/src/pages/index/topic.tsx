import React from 'react';
import  {useState,useEffect} from "react"
import {useModel} from "umi"
import styles from "./topic.less"

const topic:React.FC=(props:any)=> {
    const { getTopic} = useModel('topic', model => ({ getTopic: model.getTopic}));
    let [topiclist,setTopiclist]=useState([])
    
    useEffect(() => {
        topic()
    }, [getTopic])
    async function topic(){
        let res=await getTopic()
        setTopiclist(res.data.data)
        console.log(res.data.data)
    }
   async function todetail(id:string){
        props.history.push(`/topicDetail/${id}`)
    }
    return (
        <div>
            {
                topiclist.map((item:any)=>{
                    return <div key={item.id} className={styles.item}
                    onClick={()=>{todetail(item.id)}}>
                        <img src={item.scene_pic_url}/>
                        <span>{item.title}</span>
                        <p>{item.subtitle}</p>
                        <li>{item.price_info}元/起</li>
                    </div>
                })
            }

        </div>
    );
}

export default topic;