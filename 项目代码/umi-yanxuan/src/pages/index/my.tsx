import React from 'react';
import styles from './my.less'
import { FolderAddOutlined, SecurityScanOutlined,SmileOutlined, FormOutlined, CopyrightOutlined, EnvironmentOutlined, AccountBookOutlined, FileTextOutlined, RedEnvelopeOutlined } from '@ant-design/icons';
function My() {
    return (
        <div className={styles.my}>
            <div className={styles.head}>
                <img src={require('@/public/logo.png')} alt="" />
                <div className={styles.right}>
                    <span>1222353512</span>
                    <span>普通用户</span>
                </div>
            </div>
            <div className={styles.list}>
                <div>
                    <FolderAddOutlined />
                    <span>我的收藏</span>
                </div>
                <div>
                    <EnvironmentOutlined />
                    <span>地址管理</span>
                </div>
                <div>
                    <FileTextOutlined />
                    <span>我的订单</span>
                </div>
                <div>
                    <FolderAddOutlined />
                    <span>周末拼单</span>
                </div>
                <div>
                    <AccountBookOutlined />
                    <span>优惠券</span>
                </div>
                <div>
                    <CopyrightOutlined />
                    <span>优选购</span>
                </div>
                <div>
                    <RedEnvelopeOutlined />
                    <span>我的红包</span>
                </div>
                <div>
                    <FolderAddOutlined />
                    <span>会员plus</span>
                </div>
                <div>
                    <FolderAddOutlined />
                    <span>邀请返利</span>
                </div>
                <div>
                    <FormOutlined />
                    <span>意见反馈</span>
                </div>
                <div>
                <SmileOutlined />
                    <span>客服咨询</span>
                </div>
                <div>
                    <SecurityScanOutlined />
                    <span>账户安全</span>
                </div>
            </div>
            <button className={styles.btn}>退出登录</button>
        </div>
    );
}
My.wrappers = ['@/wrappers/auth']

export default My;