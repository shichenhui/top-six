import { Redirect } from 'umi'
import React from 'react'
const Auth:React.FC= (props) => {
    let isLogin=window.localStorage.getItem('login')
  if (isLogin) {
    return <div>{ props.children }</div>;
  } else {
    return <Redirect to="/login" />;
  }
}
export default Auth
