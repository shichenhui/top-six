import React from 'react';
import { useHistory } from 'umi'
function detailList({ite}: any) {
    let history=useHistory()
     //商品购物车详情
     function goShopDet(id: number) {
        history.push(`/shopDetail/${id}`)
        console.log(id);
    }
    return (
        <dl  onClick={() => goShopDet(ite.id)}>
            <dt>
                <img src={ite.list_pic_url} alt="" />
            </dt>
            <dd>
                <p>{ite.name.length > 14 ? ite.name.slice(0, 14) + '...' : ite.name}</p>
                <p>￥{ite.retail_price}</p>
            </dd>
        </dl>
    );
}

export default detailList;