/*
 * @Description: 
 * @Version: 2.0
 * @Autor: 张磊
 * @Date: 2020-11-19 22:13:03
 * @LastEditors: 张磊
 * @LastEditTime: 2020-11-20 10:59:05
 * @FilePath: \marker\src\models\brand.ts
 */
import {brnDetail} from '@/api/home'
import {IBrand}from '@/utils/types'
export default function getBrn(){
    async function getBrnDet(id:number):Promise<IBrand> {
        let result=await brnDetail(id)
        console.log(result,'aaaaaa');
        
        return result.data.brand
    }
    return {getBrnDet}
}