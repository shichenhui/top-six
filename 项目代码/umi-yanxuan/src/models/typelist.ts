/*
 * @Description: 
 * @Version: 2.0
 * @Autor: 张磊
 * @Date: 2020-11-19 10:51:06
 * @LastEditors: 张磊
 * @LastEditTime: 2020-11-23 08:51:35
 * @FilePath: \lht1802A\项目代码\marker\src\models\typelist.ts
 */
import { useState } from 'react'
import { type,typeSingle,addCar,getShopData,getCheck,delectShopList } from '@/api/type';
import {POrtShop,SHopList} from '@/utils/types'



export default function useUserModel(){


  async function getType(){
    let result = await type();
    return result
  }
  async function getTypeSingle(data:string){
      console.log(data,901)
      let result = await typeSingle(data)
      
      return result
  }

  async function getCar(data:POrtShop){
    let result = await addCar(data)
    console.log('zhehsi',result)
    return result
}
async function getShop(){
 let res = await getShopData()
 return res
}
const shopsPort = async (data:SHopList) =>{
  let result = await getCheck(data)
  return result
}
const delectShopData = async(data:string) =>{
    let result = await delectShopList(data)
    return result
}
  return {
    getType,
    getTypeSingle,
    getCar,
    getShop,
    shopsPort,
    delectShopData,
  }
}