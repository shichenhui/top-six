import { relate,shopDetail } from "@/api/detail"

export default function useUserModel(){
    async function getRelate(id:number):Promise<any>{
        let result = await relate(id);
        // console.log(result.data.goodsList)
        return result.data.goodsList
    }
    async function getShopDetail(id:number):Promise<any>{
        let result = await shopDetail(id);
        console.log(result.data)
        return result.data
    }

    return {
        getRelate,
        getShopDetail
    }
}