<<<<<<< HEAD
/*
 * @Descripttion: 
 * @version: 1.0
 * @Author: 任静
 * @Date: 2020-11-20 11:58:56
 * @LastEditors: 任静
 * @LastEditTime: 2020-11-24 07:47:59
 */
import { useState } from 'react'
import { login, firstList } from '@/api/user'
import { ILoginData, IDataList } from '@/util/types'
import { getlist, getCount,deleteCart,getAdd} from "@/api/user"

export default function userModul() {

    let [islogin, updateLogin] = useState(false)
    let [Idatalist] = useState(false)
    let [Iaddress] = useState(false)

    async function doLogin(data: ILoginData): Promise<boolean> {
      let result = await login(data)
      if(result.errno===0){
          updateLogin(true)
          setToken(result.data.sessionKey)
      }
      return result.errno===0
    }
    async function DataList(): Promise<any> {

        let result = await firstList()
        return result
    }
    async function getCartData(): Promise<any> {
        const res = await getlist();
        return res.data.cartList
}
async function getCartCount(): Promise<any> {
        const res = await getCount();
        return res.data.cartTotal.goodsCount
}
async function deleteCart(data:any): Promise<any> {
        const res = await deleteCart(data);
        return res.data.cartList
}

async function Address():Promise<any>{

    let result=await getAdd()
    console.log(result)
    return result
}
    return {
        islogin,
        doLogin,
        DataList,
        Idatalist,
        getCartData,getCartCount,deleteCart,
        Iaddress,
        Address
    }
}

=======
/*
 * @Description: 
 * @Version: 2.0
 * @Autor: 张磊
 * @Date: 2020-11-18 20:21:19
 * @LastEditors: 张磊
 * @LastEditTime: 2020-11-18 21:01:04
 * @FilePath: \marker\src\models\user.ts
 */
import { useState } from 'react'
import { login } from '@/api/user';
import {ILoginData} from '@/utils/types'
import {setToken} from '@/utils/index'
// import { Redirect } from 'umi'

export default function useUserModel(){
  let [isLogin, updateLogin] = useState(false);

  async function doLogin(data: ILoginData): Promise<boolean>{
    let result = await login(data);
    console.log(result,'aaaaaa');
    if (result.errno === 0){
      updateLogin(true);
      setToken(result.data.sessionKey);
    }
    return result.errno === 0;
  }

  return {
    isLogin,
    doLogin
  }
}
>>>>>>> songchao
