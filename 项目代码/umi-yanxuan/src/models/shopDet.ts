/*
 * @Description: 
 * @Version: 2.0
 * @Autor: 张磊
 * @Date: 2020-11-20 15:43:59
 * @LastEditors: 张磊
 * @LastEditTime: 2020-11-20 16:10:10
 * @FilePath: \marker\src\models\shopDet.ts
 */
import { relate,shopDetail,carCount } from '@/api/home';
export default function useUserModel(){
    async function getRelate(id:number): Promise<any>{
      let result = await relate(id);
      console.log(result.data.goodsList,'relate');
        return result.data.goodsList
    }
    async function getShopDetail(id:number): Promise<any>{
        let result = await shopDetail(id);
        console.log(result.data,'shopDetail');
          return result.data
    }
    async function getCarCount(): Promise<any>{
        let result = await carCount();
        console.log(result.data.cartTotal.goodsCount,'carCount');
          return result.data.cartTotal.goodsCount
    }
    return {
        getRelate,
        getShopDetail,
        getCarCount
    }
  }