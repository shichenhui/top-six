import { topicDetail,topicRelated } from '@/api/topic';
export default function useUserModel(){

    async function getTopicDetail(id:string	): Promise<any>{
      let result1 = await topicDetail(id);
      console.log(result1,'result1result1result1result1');
        return result1
    }
    
    async function getTopicRelated(): Promise<any>{
        let result2 = await topicRelated();
        // console.log(result2.data,'result2result2result2');
          return result2
      }
    return {
        getTopicDetail,
        getTopicRelated,
    }
  }