/*
 * @Descripttion: 
 * @version: 1.0
 * @Author: 任静
 * @Date: 2020-11-19 11:29:10
 * @LastEditors: 任静
 * @LastEditTime: 2020-11-20 20:22:41
 */
import { getlist, getCount, deleteCart ,check,getDetail} from "@/api/cart"
export default function useCartModel() {
        async function getCartData(): Promise<any> {
                const res = await getlist();
                return res.data.cartList
        }
        async function getCartCount(): Promise<any> {
                const res = await getCount();
                return res.data.cartTotal.goodsCount
        }
        async function deleteCartData(data: any): Promise<any> {
                const res = await deleteCart(data);
                return res
        }
        async function checkCart(data: any): Promise<any> {
                const res = await check(data);
                return res.data.cartList
        }
        async function getDetailData(id: any): Promise<any> {
                const res = await getDetail(id);
                return res.data
        }
        return {
                getCartData, getCartCount, deleteCartData,checkCart,getDetailData
        }
}
