import { useState } from 'react'
import { getClassList } from '@/api/classify'

export default function useUserModel(){
  let [ classList,updateClassList ] = useState([])
  async function getClassListData():Promise<any>{
    let result = await getClassList()
    return result.data
  }
  return {
    classList,
    getClassListData
  }
}