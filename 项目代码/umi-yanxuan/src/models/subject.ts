/*
 * @Author: your name
 * @Date: 2020-11-19 16:21:26
 * @LastEditTime: 2020-11-20 08:18:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \umi-yanxuan\src\models\subject.ts
 */
import {getSubjectList,getSubjectDetail,getSubjectEsti,getSubjectRecommend} from "@/api/subject"

export default ()=>{
    async function getSubjectLists(){
        let result = await getSubjectList()
        return result;
    }

    async function getSubjectDetails(id:any){
        let result = await getSubjectDetail(id)
        return result;
    }
    async function getSubjectEstis(payload:any){
        let result = await getSubjectEsti(payload)
        return result;
    }

    async function getSubjectRecommends(payload:any) {
        let result = await getSubjectRecommend(payload)
        return result;
    }
    
    return {
        getSubjectLists,
        getSubjectDetails,
        getSubjectEstis,
        getSubjectRecommends
    }
}