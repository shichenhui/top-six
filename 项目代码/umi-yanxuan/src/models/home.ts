/*
 * @Description: 
 * @Version: 2.0
 * @Autor: 张磊
 * @Date: 2020-11-18 22:02:38
 * @LastEditors: 张磊
 * @LastEditTime: 2020-11-19 22:43:15
 * @FilePath: \marker\src\models\home.ts
 */
// import { useState } from 'react'
import { getHomeList } from '@/api/home';
export default function useUserModel(){
    async function getHome(): Promise<any>{
      let result = await getHomeList();
      // console.log(result,'aaaaaa');
        return result
    }
    return {
        getHome
    }
  }