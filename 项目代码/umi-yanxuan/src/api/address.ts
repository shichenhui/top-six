/*
 * @Descripttion: 
 * @version: 1.0
 * @Author: 任静
 * @Date: 2020-11-23 20:03:26
 * @LastEditors: 任静
 * @LastEditTime: 2020-11-23 20:05:19
 */
/*
 * @Descripttion: 
 * @version: 1.0
 * @Author: 任静
 * @Date: 2020-11-19 11:26:41
 * @LastEditors: 任静
 * @LastEditTime: 2020-11-20 18:51:51
 */
import { request } from 'umi'
//购物车接口
export let getlist = () => {
    return request('/address/list', {
        method: 'GET',
    })
}
export let check = (data:any) => {
    return request('/cart/checked',
        {
            method: 'POST',
            data
        })
}
//删除
export let deleteCart = (data:any) => {
    return request('/address/delete',
        {
            method: 'POST',data
        })
}
//购物车商品数量
export let getCount = () => {
    return request('/cart/goodscount',
        {
            method: 'GET',
            
        })
}
//获取商品详情
export let getDetail = (params:any) => {
    return request('/goods/detail',
        {
            method: 'GET',
            params:params
        })
        
}
