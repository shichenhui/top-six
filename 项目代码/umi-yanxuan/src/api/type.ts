/*
 * @Description: 
 * @Version: 2.0
 * @Autor: 张磊
 * @Date: 2020-11-19 10:50:05
 * @LastEditors: 张磊
 * @LastEditTime: 2020-11-23 08:46:28
 * @FilePath: \lht1802A\项目代码\marker\src\api\type.ts
 */
import {request} from 'umi'
import {POrtShop,SHopList} from '@/utils/types'


// 登陆接口
export let type = ()=>{
  return request('/catalog/index', {
    method: 'get',
   })
}  
//类型切换 当前的内容
export let typeSingle = (id:string) =>{
  return request('/catalog/current',{
     method:'get',
     params:{id}
  })
}

//添加到购物车
export let addCar = (data:POrtShop)=>{
  console.log(data,999)
  return request('/cart/add',{
    method:'POST',
    data:data
  })
}
//获取用户购物车数据
export let getShopData = () =>{
  return request('/cart/index',{
      method:'get'
  })
}
//获取购物车商品是否选中 /cart/checked
export let getCheck = (data:SHopList) =>{
  console.log( Number(data.productId) ,2313)
  return request('/cart/checked',{
     method:'POST',
     data:data
  })
}
//删除购物车的数据
export let delectShopList = (productIds:string) =>{
  console.log(productIds)
  return request('/cart/delete',{
    method:'POST',
    data:productIds+''
  })
}
 