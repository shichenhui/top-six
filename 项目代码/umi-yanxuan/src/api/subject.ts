/*
 * @Author: your name
 * @Date: 2020-11-19 15:27:16
 * @LastEditTime: 2020-11-20 08:18:50
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \umi-yanxuan\src\api\subject.ts
 */
import {request} from "umi"

//列表
export let getSubjectList = ()=>{
    return request("/topic/list",{
        method:"get"
    })
}

//专题详情
export let getSubjectDetail = (params:any)=>{
    return request("/topic/detail",{
        method:"get",
        params:params
    })
}

//获取评价
export let getSubjectEsti = (params:any)=>{
    return request("/comment/list",{
        method:"get",
        params:params
    })
}

//推荐专题
export let getSubjectRecommend = (params:any)=>{
    return request("/topic/related",{
        method:"get",
        params:params
    })
}