import { request } from 'umi'

//分类
export let getClassList = () => {
  return request('/catalog/index',{
    method:'Get',
    params:{},
    // data:data
  })
}

export let getClassifyList = (id:number) => {
  return request(`/catalog/current?id=${id}`,{
    method:'Get',
    params:{},
    // data:data
  })
}

export let getClassifyContentList = (id:number) => {
  return request(`/goods/list?page=1&size=1000&categoryId=${id}`,{
    method:'Get',
    params:{},
    // data:data
  })
}