import { request } from 'umi'

//商品页详情接口 推荐商品数据
export const relate = (id:number)=>{
    return request('/goods/related',{
        method:'GET',
        params:{id}
    })
}

//商品页详情接口 商品详情
export const shopDetail = (id:number)=>{
    return request('/goods/detail',{
        method:'GET',
        params:{id}
    })
}

