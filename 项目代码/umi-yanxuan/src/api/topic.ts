import {request} from "umi"
import {TYpeData} from "@/utils/types"

//获取专题数据接口
export let topic=()=>{
    return request('/topic/list',{
        method:'GET',
        params:{},
    })
} 
//获取专题详情
export let topicDetail=(id:string)=>{
    return request('/topic/detail',{
        method:'GET',
        params:{id},
    })
} 

//获取相关专题
export let topicRelated=()=>{
    return request('/topic/related',{
        method:'GET',
        params:{},
    })
} 

//根据专题ID或者商品ID获取评论获取相关专题
export let CommentList=()=>{
    return request('/comment/list',{
        method:'GET',
        params:{},
    })
} 
