/*
 * @Description: 
 * @Version: 2.0
 * @Autor: 张磊
 * @Date: 2020-11-18 21:57:49
 * @LastEditors: 张磊
 * @LastEditTime: 2020-11-20 16:02:01
 * @FilePath: \marker\src\api\home.ts
 */
import {request} from 'umi'

//首页数据接口
export const getHomeList=()=>{
    return request('/',{
        method:'GET',
        params:{}
    })
}

//制造商详情接口
export const brnDetail=(id:number)=>{
    return request('/brand/detail',{
        method:'GET',
        params:{id}
    })
}

//商品页详情接口 推荐商品数据
export const relate=(id:number)=>{
    return request('/goods/related',{
        method:'GET',
        params:{id}
    })
}
//商品页详情接口 商品详情
export const shopDetail=(id:number)=>{
    return request('/goods/detail',{
        method:'GET',
        params:{id}
    })
}
//商品页详情接口 购物车商品数量
export const carCount=()=>{
    return request('/cart/goodscount',{
        method:'GET',
        params:{}
    })
}