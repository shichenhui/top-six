//登录参数类型接口
export interface ILoginData {
    mobile: string,
    password: string
}
//分类id参数接口
export interface TYpeData {
    id: string
}
//制造商详情接口
export interface IBrand {
    app_list_pic_url: string
    floor_price: number
    id: number
    is_new: number
    is_show: number
    list_pic_url: string
    name: string
    new_pic_url: string
    new_sort_order: number
    pic_url: string
    simple_desc: string
    sort_order: number
}

export interface POrtShop{
    goodsId:string,
    number:number,
    productId:number
}
export interface SHopList{
    isChecked:number,
    productId:string
}