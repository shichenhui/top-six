
import { RequestConfig } from 'umi';
import {RequestOptionsInit} from 'umi-request'
import {getToken} from '@/utils/index'

export const request: RequestConfig = {
  timeout: 3000,
  prefix: 'http://easymarket.jasonandjay.com',
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [(url:string, options:RequestOptionsInit)=>{
    let token = getToken();
    let headers = options.headers;
    if (token){
      headers = {...options.headers, 'x-nideshop-token': token}
    }
    return {
      url,
      options: { ...options, headers},
    };
  }],
  responseInterceptors: [],
};
